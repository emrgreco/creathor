<?php
include("conexion.php");


$SITIO="active";
$TABLA="";
$CAMPO="";

unset($_SESSION);

?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos de SITIOS</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">

	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include('nav.php');?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Lista de sitios</h2>
			<hr />

			<?php
			if(isset($_GET['aksi']) == 'delete'){
				// escaping, campo_additionally removing everything that could be (html/javascript-) code
				$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
				$cek = mysqli_query($con, "SELECT * FROM sitio WHERE id_sitio='$nik'");
				if(mysqli_num_rows($cek) == 0){
					echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
				}else{
					$delete = mysqli_query($con, "DELETE FROM sitio WHERE id_sitio='$nik'");
					$delete = mysqli_query($con, "DELETE FROM tabla WHERE id_sitio='$nik'");
					$delete = mysqli_query($con, "DELETE FROM campos WHERE id_sitio='$nik'");
					if($delete){
						echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
					}else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.</div>';
					}
				}
			}
			?>
			<a href="sitio_add.php">Agregar sitio</a>
			<br />
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
                    <th>num</th>
					<th>Clave</th>
					<th>Nombre</th>
                    <th>descripción</th>
                    <th>Acciones</th>
				</tr>
				<?php

				$sql = mysqli_query($con, "SELECT * FROM sitio");
				if(mysqli_num_rows($sql) == 0){
					echo '<tr><td colspan="8">No hay datos.</td></tr>';
				}else{
					$no = 1;
					while($row = mysqli_fetch_assoc($sql)){
						echo '
						<tr>
							<td>'.$no.'</td>
							<td><a href="TABLAS.php?SITE='.$row['id_sitio'].'">'.$row['clave_sitio'].'</a></td>
							<td>'.$row['nombre_sitio'].'</a></td>
							<td>'.$row['descripcion_sitio'].'</td>';
						echo '
							<td>

								<a href="SITES/'.$row['clave_sitio'].'/" title="campo_editar datos" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>

								<a href="sitio_edit.php?nik='.$row['id_sitio'].'" title="campo_editar datos" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
								<a href="SITIOS.php?aksi=delete&nik='.$row['id_sitio'].'" title="Eliminar" onclick="return confirm(\'Esta seguro de borrar los datos '.$row['nombre_sitio'].'?\')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
								<a href="index.php?SITE='.$row['id_sitio'].'" title="Eliminar" onclick="return class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>
							</td>
						</tr>
						';
						$no++;
					}
				}
				?>
			</table>
			</div>
		</div>
	</div><center>
	<p>&copy; diseño <?php echo date("Y");?></p
		</center>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
