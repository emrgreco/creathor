<?php
include("conexion.php");
if(!isset($_GET['SITE']))
{
	header("Location: tablaS.php");
}
else
{
	$site=$_GET['SITE'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos del tabla</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
	
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del tablaS &raquo; campo_editar datos</h2>
			<hr />
			
			<?php
			// escaping, campo_additionally removing everything that could be (html/javascript-) code
			$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
			$sql = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$nik'");
			if(mysqli_num_rows($sql) == 0){
				header("Location: TABLAS.php?SITE=$site");
			}else{
				$row = mysqli_fetch_assoc($sql);
			}
			if(isset($_POST['save'])){
				$clave_tabla		     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_tabla	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$icono	 = mysqli_real_escape_string($con,(strip_tags($_POST["icono"],ENT_QUOTES)));//Escanpando caracteres 
				$desplegable	 = mysqli_real_escape_string($con,(strip_tags($_POST["desplegable"],ENT_QUOTES)));//Escanpando 
				$tabla_principal	 = mysqli_real_escape_string($con,(strip_tags($_POST["tabla_principal"],ENT_QUOTES)));//Escanpando caracteres 
				$tipo_tabla	 = mysqli_real_escape_string($con,(strip_tags($_POST["tipo_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$tabla_liga	 = mysqli_real_escape_string($con,(strip_tags($_POST["tabla_liga"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at	 =  date("Y-m-d H:i:s"); 
			

				$update = mysqli_query($con, "UPDATE tabla SET id_sitio='$site', clave_tabla='$clave_tabla',  descripcion_tabla='$descripcion_tabla', desplegable='$desplegable', icono='$icono', update_at='$update_at' , tabla_principal = '$tabla_principal', tipo_tabla='$tipo_tabla', tabla_liga='$tabla_liga' WHERE id_tabla='$nik'") or die(mysqli_error());
				if($update){
					header("Location: TABLAS.php?SITE=".$site);
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
				}
			}
			
			if(isset($_GET['pesan']) == 'sukses'){
				echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
							header("Location: tablaS.php");	
			}
			?>
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">clave</label>
					<div class="col-sm-2">
						<input type="text" name="clave_tabla" value="<?php echo $row ['clave_tabla']; ?>" class="form-control" placeholder="NIK" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_tabla</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_tabla" value="<?php echo $row ['descripcion_tabla']; ?>" class="form-control" placeholder="descripcion_tabla" required>
					</div>
				</div>



				<div class="form-group">
					<label class="col-sm-3 control-label">icono</label>
					<div class="col-sm-3">
						<select name="icono" class="form-control selectpicker">
							<option value="">- Selecciona icono -</option>
							<option data-icon="glyphicon glyphicon-asterisk" value='1' <?php if($row['icono']==1) echo 'selected'; ?>>glyphicon-asterisk</option>
							<option data-icon="glyphicon glyphicon-plus" value='2' <?php if($row['icono']==2) echo 'selected'; ?>>glyphicon-plus</option>
							<option data-icon="glyphicon glyphicon-euro" value='3' <?php if($row['icono']==3) echo 'selected'; ?>>glyphicon-euro</option>
							<option data-icon="glyphicon glyphicon-eur" value='4' <?php if($row['icono']==4) echo 'selected'; ?>>glyphicon-eur</option>
							<option data-icon="glyphicon glyphicon-minus" value='5' <?php if($row['icono']==5) echo 'selected'; ?>>glyphicon-minus</option>
							<option data-icon="glyphicon glyphicon-cloud" value='6' <?php if($row['icono']==6) echo 'selected'; ?>>glyphicon-cloud</option>
							<option data-icon="glyphicon glyphicon-envelope" value='7' <?php if($row['icono']==7) echo 'selected'; ?>>glyphicon-envelope</option>
							<option data-icon="glyphicon glyphicon-pencil" value='8' <?php if($row['icono']==8) echo 'selected'; ?>>glyphicon-pencil</option>
							<option data-icon="glyphicon glyphicon-glass" value='9' <?php if($row['icono']==9) echo 'selected'; ?>>glyphicon-glass</option>
							<option data-icon="glyphicon glyphicon-music" value='10' <?php if($row['icono']==10) echo 'selected'; ?>>glyphicon-music</option>
							<option data-icon="glyphicon glyphicon-search" value='11' <?php if($row['icono']==11) echo 'selected'; ?>>glyphicon-search</option>
							<option data-icon="glyphicon glyphicon-heart" value='12' <?php if($row['icono']==12) echo 'selected'; ?>>glyphicon-heart</option>
							<option data-icon="glyphicon glyphicon-star" value='13' <?php if($row['icono']==13) echo 'selected'; ?>>glyphicon-star</option>
							<option data-icon="glyphicon glyphicon-star-empty" value='14' <?php if($row['icono']==14) echo 'selected'; ?>>glyphicon-star-empty</option>
							<option data-icon="glyphicon glyphicon-user" value='15' <?php if($row['icono']==15) echo 'selected'; ?>>glyphicon-user</option>
							<option data-icon="glyphicon glyphicon-film" value='16' <?php if($row['icono']==16) echo 'selected'; ?>>glyphicon-film</option>
							<option data-icon="glyphicon glyphicon-th-large" value='17' <?php if($row['icono']==17) echo 'selected'; ?>>glyphicon-th-large</option>
							<option data-icon="glyphicon glyphicon-th" value='18' <?php if($row['icono']==18) echo 'selected'; ?>>glyphicon-th</option>
							<option data-icon="glyphicon glyphicon-th-list" value='19' <?php if($row['icono']==19) echo 'selected'; ?>>glyphicon-th-list</option>
							<option data-icon="glyphicon glyphicon-ok" value='20' <?php if($row['icono']==20) echo 'selected'; ?>>glyphicon-ok</option>
							<option data-icon="glyphicon glyphicon-remove" value='21' <?php if($row['icono']==21) echo 'selected'; ?>>glyphicon-remove</option>
							<option data-icon="glyphicon glyphicon-zoom-in" value='22' <?php if($row['icono']==22) echo 'selected'; ?>>glyphicon-zoom-in</option>
							<option data-icon="glyphicon glyphicon-zoom-out" value='23' <?php if($row['icono']==23) echo 'selected'; ?>>glyphicon-zoom-out</option>
							<option data-icon="glyphicon glyphicon-off" value='24' <?php if($row['icono']==24) echo 'selected'; ?>>glyphicon-off</option>
							<option data-icon="glyphicon glyphicon-signal" value='25' <?php if($row['icono']==25) echo 'selected'; ?>>glyphicon-signal</option>
							<option data-icon="glyphicon glyphicon-cog" value='26' <?php if($row['icono']==26) echo 'selected'; ?>>glyphicon-cog</option>
							<option data-icon="glyphicon glyphicon-trash" value='27' <?php if($row['icono']==27) echo 'selected'; ?>>glyphicon-trash</option>
							<option data-icon="glyphicon glyphicon-home" value='28' <?php if($row['icono']==28) echo 'selected'; ?>>glyphicon-home</option>
							<option data-icon="glyphicon glyphicon-file" value='29' <?php if($row['icono']==29) echo 'selected'; ?>>glyphicon-file</option>
							<option data-icon="glyphicon glyphicon-time" value='30' <?php if($row['icono']==30) echo 'selected'; ?>>glyphicon-time</option>
							<option data-icon="glyphicon glyphicon-road" value='31' <?php if($row['icono']==31) echo 'selected'; ?>>glyphicon-road</option>
							<option data-icon="glyphicon glyphicon-download-alt" value='32' <?php if($row['icono']==32) echo 'selected'; ?>>glyphicon-download-alt</option>
							<option data-icon="glyphicon glyphicon-download" value='33' <?php if($row['icono']==33) echo 'selected'; ?>>glyphicon-download</option>
							<option data-icon="glyphicon glyphicon-upload" value='34' <?php if($row['icono']==34) echo 'selected'; ?>>glyphicon-upload</option>
							<option data-icon="glyphicon glyphicon-inbox" value='35' <?php if($row['icono']==35) echo 'selected'; ?>>glyphicon-inbox</option>
							<option data-icon="glyphicon glyphicon-play-circle" value='36' <?php if($row['icono']==36) echo 'selected'; ?>>glyphicon-play-circle</option>
							<option data-icon="glyphicon glyphicon-repeat" value='37' <?php if($row['icono']==37) echo 'selected'; ?>>glyphicon-repeat</option>
							<option data-icon="glyphicon glyphicon-refresh" value='38' <?php if($row['icono']==38) echo 'selected'; ?>>glyphicon-refresh</option>
							<option data-icon="glyphicon glyphicon-list-alt" value='39' <?php if($row['icono']==39) echo 'selected'; ?>>glyphicon-list-alt</option>
							<option data-icon="glyphicon glyphicon-lock" value='40' <?php if($row['icono']==40) echo 'selected'; ?>>glyphicon-lock</option>
							<option data-icon="glyphicon glyphicon-flag" value='41' <?php if($row['icono']==41) echo 'selected'; ?>>glyphicon-flag</option>
							<option data-icon="glyphicon glyphicon-headphones" value='42' <?php if($row['icono']==42) echo 'selected'; ?>>glyphicon-headphones</option>
							<option data-icon="glyphicon glyphicon-volume-off" value='43' <?php if($row['icono']==43) echo 'selected'; ?>>glyphicon-volume-off</option>
							<option data-icon="glyphicon glyphicon-volume-down" value='44' <?php if($row['icono']==44) echo 'selected'; ?>>glyphicon-volume-down</option>
							<option data-icon="glyphicon glyphicon-volume-up" value='45' <?php if($row['icono']==45) echo 'selected'; ?>>glyphicon-volume-up</option>
							<option data-icon="glyphicon glyphicon-qrcode" value='46' <?php if($row['icono']==46) echo 'selected'; ?>>glyphicon-qrcode</option>
							<option data-icon="glyphicon glyphicon-barcode" value='47' <?php if($row['icono']==47) echo 'selected'; ?>>glyphicon-barcode</option>
							<option data-icon="glyphicon glyphicon-tag" value='48' <?php if($row['icono']==48) echo 'selected'; ?>>glyphicon-tag</option>
							<option data-icon="glyphicon glyphicon-tags" value='49' <?php if($row['icono']==49) echo 'selected'; ?>>glyphicon-tags</option>
							<option data-icon="glyphicon glyphicon-book" value='50' <?php if($row['icono']==50) echo 'selected'; ?>>glyphicon-book</option>
							<option data-icon="glyphicon glyphicon-bookmark" value='51' <?php if($row['icono']==51) echo 'selected'; ?>>glyphicon-bookmark</option>
							<option data-icon="glyphicon glyphicon-print" value='52' <?php if($row['icono']==52) echo 'selected'; ?>>glyphicon-print</option>
							<option data-icon="glyphicon glyphicon-camera" value='53' <?php if($row['icono']==53) echo 'selected'; ?>>glyphicon-camera</option>
							<option data-icon="glyphicon glyphicon-font" value='54' <?php if($row['icono']==54) echo 'selected'; ?>>glyphicon-font</option>
							<option data-icon="glyphicon glyphicon-bold" value='55' <?php if($row['icono']==55) echo 'selected'; ?>>glyphicon-bold</option>
							<option data-icon="glyphicon glyphicon-italic" value='56' <?php if($row['icono']==56) echo 'selected'; ?>>glyphicon-italic</option>
							<option data-icon="glyphicon glyphicon-text-height" value='57' <?php if($row['icono']==57) echo 'selected'; ?>>glyphicon-text-height</option>
							<option data-icon="glyphicon glyphicon-text-width" value='58' <?php if($row['icono']==58) echo 'selected'; ?>>glyphicon-text-width</option>
							<option data-icon="glyphicon glyphicon-align-left" value='59' <?php if($row['icono']==59) echo 'selected'; ?>>glyphicon-align-left</option>
							<option data-icon="glyphicon glyphicon-align-center" value='60' <?php if($row['icono']==60) echo 'selected'; ?>>glyphicon-align-center</option>
							<option data-icon="glyphicon glyphicon-align-right" value='61' <?php if($row['icono']==61) echo 'selected'; ?>>glyphicon-align-right</option>
							<option data-icon="glyphicon glyphicon-align-justify" value='62' <?php if($row['icono']==62) echo 'selected'; ?>>glyphicon-align-justify</option>
							<option data-icon="glyphicon glyphicon-list" value='63' <?php if($row['icono']==63) echo 'selected'; ?>>glyphicon-list</option>
							<option data-icon="glyphicon glyphicon-indent-left" value='64' <?php if($row['icono']==64) echo 'selected'; ?>>glyphicon-indent-left</option>
							<option data-icon="glyphicon glyphicon-indent-right" value='65' <?php if($row['icono']==65) echo 'selected'; ?>>glyphicon-indent-right</option>
							<option data-icon="glyphicon glyphicon-facetime-video" value='66' <?php if($row['icono']==66) echo 'selected'; ?>>glyphicon-facetime-video</option>
							<option data-icon="glyphicon glyphicon-picture" value='67' <?php if($row['icono']==67) echo 'selected'; ?>>glyphicon-picture</option>
							<option data-icon="glyphicon glyphicon-map-marker" value='68' <?php if($row['icono']==68) echo 'selected'; ?>>glyphicon-map-marker</option>
							<option data-icon="glyphicon glyphicon-adjust" value='69' <?php if($row['icono']==69) echo 'selected'; ?>>glyphicon-adjust</option>
							<option data-icon="glyphicon glyphicon-tint" value='70' <?php if($row['icono']==70) echo 'selected'; ?>>glyphicon-tint</option>
							<option data-icon="glyphicon glyphicon-edit" value='71' <?php if($row['icono']==71) echo 'selected'; ?>>glyphicon-edit</option>
							<option data-icon="glyphicon glyphicon-share" value='72' <?php if($row['icono']==72) echo 'selected'; ?>>glyphicon-share</option>
							<option data-icon="glyphicon glyphicon-check" value='73' <?php if($row['icono']==73) echo 'selected'; ?>>glyphicon-check</option>
							<option data-icon="glyphicon glyphicon-move" value='74' <?php if($row['icono']==74) echo 'selected'; ?>>glyphicon-move</option>
							<option data-icon="glyphicon glyphicon-step-backward" value='75' <?php if($row['icono']==75) echo 'selected'; ?>>glyphicon-step-backward</option>
							<option data-icon="glyphicon glyphicon-fast-backward" value='76' <?php if($row['icono']==76) echo 'selected'; ?>>glyphicon-fast-backward</option>
							<option data-icon="glyphicon glyphicon-backward" value='77' <?php if($row['icono']==77) echo 'selected'; ?>>glyphicon-backward</option>
							<option data-icon="glyphicon glyphicon-play" value='78' <?php if($row['icono']==78) echo 'selected'; ?>>glyphicon-play</option>
							<option data-icon="glyphicon glyphicon-pause" value='79' <?php if($row['icono']==79) echo 'selected'; ?>>glyphicon-pause</option>
							<option data-icon="glyphicon glyphicon-stop" value='80' <?php if($row['icono']==80) echo 'selected'; ?>>glyphicon-stop</option>
							<option data-icon="glyphicon glyphicon-forward" value='81' <?php if($row['icono']==81) echo 'selected'; ?>>glyphicon-forward</option>
							<option data-icon="glyphicon glyphicon-fast-forward" value='82' <?php if($row['icono']==82) echo 'selected'; ?>>glyphicon-fast-forward</option>
							<option data-icon="glyphicon glyphicon-step-forward" value='83' <?php if($row['icono']==83) echo 'selected'; ?>>glyphicon-step-forward</option>
							<option data-icon="glyphicon glyphicon-eject" value='84' <?php if($row['icono']==84) echo 'selected'; ?>>glyphicon-eject</option>
							<option data-icon="glyphicon glyphicon-chevron-left" value='85' <?php if($row['icono']==85) echo 'selected'; ?>>glyphicon-chevron-left</option>
							<option data-icon="glyphicon glyphicon-chevron-right" value='86' <?php if($row['icono']==86) echo 'selected'; ?>>glyphicon-chevron-right</option>
							<option data-icon="glyphicon glyphicon-plus-sign" value='87' <?php if($row['icono']==87) echo 'selected'; ?>>glyphicon-plus-sign</option>
							<option data-icon="glyphicon glyphicon-minus-sign" value='88' <?php if($row['icono']==88) echo 'selected'; ?>>glyphicon-minus-sign</option>
							<option data-icon="glyphicon glyphicon-remove-sign" value='89' <?php if($row['icono']==89) echo 'selected'; ?>>glyphicon-remove-sign</option>
							<option data-icon="glyphicon glyphicon-ok-sign" value='90' <?php if($row['icono']==90) echo 'selected'; ?>>glyphicon-ok-sign</option>
							<option data-icon="glyphicon glyphicon-question-sign" value='91' <?php if($row['icono']==91) echo 'selected'; ?>>glyphicon-question-sign</option>
							<option data-icon="glyphicon glyphicon-info-sign" value='92' <?php if($row['icono']==92) echo 'selected'; ?>>glyphicon-info-sign</option>
							<option data-icon="glyphicon glyphicon-screenshot" value='93' <?php if($row['icono']==93) echo 'selected'; ?>>glyphicon-screenshot</option>
							<option data-icon="glyphicon glyphicon-remove-circle" value='94' <?php if($row['icono']==94) echo 'selected'; ?>>glyphicon-remove-circle</option>
							<option data-icon="glyphicon glyphicon-ok-circle" value='95' <?php if($row['icono']==95) echo 'selected'; ?>>glyphicon-ok-circle</option>
							<option data-icon="glyphicon glyphicon-ban-circle" value='96' <?php if($row['icono']==96) echo 'selected'; ?>>glyphicon-ban-circle</option>
							<option data-icon="glyphicon glyphicon-arrow-left" value='97' <?php if($row['icono']==97) echo 'selected'; ?>>glyphicon-arrow-left</option>
							<option data-icon="glyphicon glyphicon-arrow-right" value='98' <?php if($row['icono']==98) echo 'selected'; ?>>glyphicon-arrow-right</option>
							<option data-icon="glyphicon glyphicon-arrow-up" value='99' <?php if($row['icono']==99) echo 'selected'; ?>>glyphicon-arrow-up</option>
							<option data-icon="glyphicon glyphicon-arrow-down" value='100' <?php if($row['icono']==100) echo 'selected'; ?>>glyphicon-arrow-down</option>
							<option data-icon="glyphicon glyphicon-share-alt" value='101' <?php if($row['icono']==101) echo 'selected'; ?>>glyphicon-share-alt</option>
							<option data-icon="glyphicon glyphicon-resize-full" value='102' <?php if($row['icono']==102) echo 'selected'; ?>>glyphicon-resize-full</option>
							<option data-icon="glyphicon glyphicon-resize-small" value='103' <?php if($row['icono']==103) echo 'selected'; ?>>glyphicon-resize-small</option>
							<option data-icon="glyphicon glyphicon-exclamation-sign" value='104' <?php if($row['icono']==104) echo 'selected'; ?>>glyphicon-exclamation-sign</option>
							<option data-icon="glyphicon glyphicon-gift" value='105' <?php if($row['icono']==105) echo 'selected'; ?>>glyphicon-gift</option>
							<option data-icon="glyphicon glyphicon-leaf" value='106' <?php if($row['icono']==106) echo 'selected'; ?>>glyphicon-leaf</option>
							<option data-icon="glyphicon glyphicon-fire" value='107' <?php if($row['icono']==107) echo 'selected'; ?>>glyphicon-fire</option>
							<option data-icon="glyphicon glyphicon-eye-open" value='108' <?php if($row['icono']==108) echo 'selected'; ?>>glyphicon-eye-open</option>
							<option data-icon="glyphicon glyphicon-eye-close" value='109' <?php if($row['icono']==109) echo 'selected'; ?>>glyphicon-eye-close</option>
							<option data-icon="glyphicon glyphicon-warning-sign" value='110' <?php if($row['icono']==110) echo 'selected'; ?>>glyphicon-warning-sign</option>
							<option data-icon="glyphicon glyphicon-plane" value='111' <?php if($row['icono']==111) echo 'selected'; ?>>glyphicon-plane</option>
							<option data-icon="glyphicon glyphicon-calendar" value='112' <?php if($row['icono']==112) echo 'selected'; ?>>glyphicon-calendar</option>
							<option data-icon="glyphicon glyphicon-random" value='113' <?php if($row['icono']==113) echo 'selected'; ?>>glyphicon-rando</option>
							<option data-icon="glyphicon glyphicon-comment" value='114' <?php if($row['icono']==114) echo 'selected'; ?>>glyphicon-commen</option>
							<option data-icon="glyphicon glyphicon-magnet" value='115' <?php if($row['icono']==115) echo 'selected'; ?>>glyphicon-magne</option>
							<option data-icon="glyphicon glyphicon-chevron-up" value='116' <?php if($row['icono']==116) echo 'selected'; ?>>glyphicon-chevron-u</option>
							<option data-icon="glyphicon glyphicon-chevron-down" value='117' <?php if($row['icono']==117) echo 'selected'; ?>>glyphicon-chevron-dow</option>
							<option data-icon="glyphicon glyphicon-retweet" value='118' <?php if($row['icono']==118) echo 'selected'; ?>>glyphicon-retwee</option>
							<option data-icon="glyphicon glyphicon-shopping-cart" value='119' <?php if($row['icono']==119) echo 'selected'; ?>>glyphicon-shopping-car</option>
							<option data-icon="glyphicon glyphicon-folder-close" value='120' <?php if($row['icono']==120) echo 'selected'; ?>>glyphicon-folder-clos</option>
							<option data-icon="glyphicon glyphicon-folder-open" value='121' <?php if($row['icono']==121) echo 'selected'; ?>>glyphicon-folder-ope</option>
							<option data-icon="glyphicon glyphicon-resize-vertical" value='122' <?php if($row['icono']==122) echo 'selected'; ?>>glyphicon-resize-vertica</option>
							<option data-icon="glyphicon glyphicon-resize-horizontal" value='123' <?php if($row['icono']==123) echo 'selected'; ?>>glyphicon-resize-horizonta</option>
							<option data-icon="glyphicon glyphicon-hdd" value='124' <?php if($row['icono']==124) echo 'selected'; ?>>glyphicon-hd</option>
							<option data-icon="glyphicon glyphicon-bullhorn" value='125' <?php if($row['icono']==125) echo 'selected'; ?>>glyphicon-bullhor</option>
							<option data-icon="glyphicon glyphicon-bell" value='126' <?php if($row['icono']==126) echo 'selected'; ?>>glyphicon-bel</option>
							<option data-icon="glyphicon glyphicon-certificate" value='127' <?php if($row['icono']==127) echo 'selected'; ?>>glyphicon-certificat</option>
							<option data-icon="glyphicon glyphicon-thumbs-up" value='128' <?php if($row['icono']==128) echo 'selected'; ?>>glyphicon-thumbs-u</option>
							<option data-icon="glyphicon glyphicon-thumbs-down" value='129' <?php if($row['icono']==129) echo 'selected'; ?>>glyphicon-thumbs-dow</option>
							<option data-icon="glyphicon glyphicon-hand-right" value='130' <?php if($row['icono']==130) echo 'selected'; ?>>glyphicon-hand-righ</option>
							<option data-icon="glyphicon glyphicon-hand-left" value='131' <?php if($row['icono']==131) echo 'selected'; ?>>glyphicon-hand-lef</option>
							<option data-icon="glyphicon glyphicon-hand-up" value='132' <?php if($row['icono']==132) echo 'selected'; ?>>glyphicon-hand-u</option>
							<option data-icon="glyphicon glyphicon-hand-down" value='133' <?php if($row['icono']==133) echo 'selected'; ?>>glyphicon-hand-dow</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-right" value='134' <?php if($row['icono']==134) echo 'selected'; ?>>glyphicon-circle-arrow-righ</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-left" value='135' <?php if($row['icono']==135) echo 'selected'; ?>>glyphicon-circle-arrow-lef</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-up" value='136' <?php if($row['icono']==136) echo 'selected'; ?>>glyphicon-circle-arrow-u</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-down" value='137' <?php if($row['icono']==137) echo 'selected'; ?>>glyphicon-circle-arrow-dow</option>
							<option data-icon="glyphicon glyphicon-globe" value='138' <?php if($row['icono']==138) echo 'selected'; ?>>glyphicon-glob</option>
							<option data-icon="glyphicon glyphicon-wrench" value='139' <?php if($row['icono']==139) echo 'selected'; ?>>glyphicon-wrenc</option>
							<option data-icon="glyphicon glyphicon-tasks" value='140' <?php if($row['icono']==140) echo 'selected'; ?>>glyphicon-task</option>
							<option data-icon="glyphicon glyphicon-filter" value='141' <?php if($row['icono']==141) echo 'selected'; ?>>glyphicon-filte</option>
							<option data-icon="glyphicon glyphicon-briefcase" value='142' <?php if($row['icono']==142) echo 'selected'; ?>>glyphicon-briefcas</option>
							<option data-icon="glyphicon glyphicon-fullscreen" value='143' <?php if($row['icono']==143) echo 'selected'; ?>>glyphicon-fullscree</option>
							<option data-icon="glyphicon glyphicon-dashboard" value='144' <?php if($row['icono']==144) echo 'selected'; ?>>glyphicon-dashboar</option>
							<option data-icon="glyphicon glyphicon-paperclip" value='145' <?php if($row['icono']==145) echo 'selected'; ?>>glyphicon-papercli</option>
							<option data-icon="glyphicon glyphicon-heart-empty" value='146' <?php if($row['icono']==146) echo 'selected'; ?>>glyphicon-heart-empt</option>
							<option data-icon="glyphicon glyphicon-link" value='147' <?php if($row['icono']==147) echo 'selected'; ?>>glyphicon-lin</option>
							<option data-icon="glyphicon glyphicon-phone" value='148' <?php if($row['icono']==148) echo 'selected'; ?>>glyphicon-phon</option>
							<option data-icon="glyphicon glyphicon-pushpin" value='149' <?php if($row['icono']==149) echo 'selected'; ?>>glyphicon-pushpi</option>
							<option data-icon="glyphicon glyphicon-usd" value='150' <?php if($row['icono']==150) echo 'selected'; ?>>glyphicon-us</option>
							<option data-icon="glyphicon glyphicon-gbp" value='151' <?php if($row['icono']==151) echo 'selected'; ?>>glyphicon-gb</option>
							<option data-icon="glyphicon glyphicon-sort" value='152' <?php if($row['icono']==152) echo 'selected'; ?>>glyphicon-sor</option>
							<option data-icon="glyphicon glyphicon-sort-by-alphabet" value='153' <?php if($row['icono']==153) echo 'selected'; ?>>glyphicon-sort-by-alphabe</option>
							<option data-icon="glyphicon glyphicon-sort-by-alphabet-alt" value='154' <?php if($row['icono']==154) echo 'selected'; ?>>glyphicon-sort-by-alphabet-al</option>
							<option data-icon="glyphicon glyphicon-sort-by-order" value='155' <?php if($row['icono']==155) echo 'selected'; ?>>glyphicon-sort-by-orde</option>
							<option data-icon="glyphicon glyphicon-sort-by-order-alt" value='156' <?php if($row['icono']==156) echo 'selected'; ?>>glyphicon-sort-by-order-al</option>
							<option data-icon="glyphicon glyphicon-sort-by-attributes" value='157' <?php if($row['icono']==157) echo 'selected'; ?>>glyphicon-sort-by-attribute</option>
							<option data-icon="glyphicon glyphicon-sort-by-attributes-alt" value='158' <?php if($row['icono']==158) echo 'selected'; ?>>glyphicon-sort-by-attributes-al</option>
							<option data-icon="glyphicon glyphicon-unchecked" value='159' <?php if($row['icono']==159) echo 'selected'; ?>>glyphicon-unchecke</option>
							<option data-icon="glyphicon glyphicon-expand" value='160' <?php if($row['icono']==160) echo 'selected'; ?>>glyphicon-expan</option>
							<option data-icon="glyphicon glyphicon-collapse-down" value='161' <?php if($row['icono']==161) echo 'selected'; ?>>glyphicon-collapse-dow</option>
							<option data-icon="glyphicon glyphicon-collapse-up" value='162' <?php if($row['icono']==162) echo 'selected'; ?>>glyphicon-collapse-u</option>
							<option data-icon="glyphicon glyphicon-log-in" value='163' <?php if($row['icono']==163) echo 'selected'; ?>>glyphicon-log-i</option>
							<option data-icon="glyphicon glyphicon-flash" value='164' <?php if($row['icono']==164) echo 'selected'; ?>>glyphicon-flas</option>
							<option data-icon="glyphicon glyphicon-log-out" value='165' <?php if($row['icono']==165) echo 'selected'; ?>>glyphicon-log-ou</option>
							<option data-icon="glyphicon glyphicon-new-window" value='166' <?php if($row['icono']==166) echo 'selected'; ?>>glyphicon-new-windo</option>
							<option data-icon="glyphicon glyphicon-record" value='167' <?php if($row['icono']==167) echo 'selected'; ?>>glyphicon-recor</option>
							<option data-icon="glyphicon glyphicon-save" value='168' <?php if($row['icono']==168) echo 'selected'; ?>>glyphicon-sav</option>
							<option data-icon="glyphicon glyphicon-open" value='169' <?php if($row['icono']==169) echo 'selected'; ?>>glyphicon-ope</option>
							<option data-icon="glyphicon glyphicon-saved" value='170' <?php if($row['icono']==170) echo 'selected'; ?>>glyphicon-save</option>
							<option data-icon="glyphicon glyphicon-import" value='171' <?php if($row['icono']==171) echo 'selected'; ?>>glyphicon-impor</option>
							<option data-icon="glyphicon glyphicon-export" value='172' <?php if($row['icono']==172) echo 'selected'; ?>>glyphicon-expor</option>
							<option data-icon="glyphicon glyphicon-send" value='173' <?php if($row['icono']==173) echo 'selected'; ?>>glyphicon-sen</option>
							<option data-icon="glyphicon glyphicon-floppy-disk" value='174' <?php if($row['icono']==174) echo 'selected'; ?>>glyphicon-floppy-dis</option>
							<option data-icon="glyphicon glyphicon-floppy-saved" value='175' <?php if($row['icono']==175) echo 'selected'; ?>>glyphicon-floppy-save</option>
							<option data-icon="glyphicon glyphicon-floppy-remove" value='176' <?php if($row['icono']==176) echo 'selected'; ?>>glyphicon-floppy-remov</option>
							<option data-icon="glyphicon glyphicon-floppy-save" value='177' <?php if($row['icono']==177) echo 'selected'; ?>>glyphicon-floppy-sav</option>
							<option data-icon="glyphicon glyphicon-floppy-open" value='178' <?php if($row['icono']==178) echo 'selected'; ?>>glyphicon-floppy-ope</option>
							<option data-icon="glyphicon glyphicon-credit-card" value='179' <?php if($row['icono']==179) echo 'selected'; ?>>glyphicon-credit-car</option>
							<option data-icon="glyphicon glyphicon-transfer" value='180' <?php if($row['icono']==180) echo 'selected'; ?>>glyphicon-transfe</option>
							<option data-icon="glyphicon glyphicon-cutlery" value='181' <?php if($row['icono']==181) echo 'selected'; ?>>glyphicon-cutler</option>
							<option data-icon="glyphicon glyphicon-header" value='182' <?php if($row['icono']==182) echo 'selected'; ?>>glyphicon-heade</option>
							<option data-icon="glyphicon glyphicon-compressed" value='183' <?php if($row['icono']==183) echo 'selected'; ?>>glyphicon-compresse</option>
							<option data-icon="glyphicon glyphicon-earphone" value='184' <?php if($row['icono']==184) echo 'selected'; ?>>glyphicon-earphon</option>
							<option data-icon="glyphicon glyphicon-phone-alt" value='185' <?php if($row['icono']==185) echo 'selected'; ?>>glyphicon-phone-al</option>
							<option data-icon="glyphicon glyphicon-tower" value='186' <?php if($row['icono']==186) echo 'selected'; ?>>glyphicon-towe</option>
							<option data-icon="glyphicon glyphicon-stats" value='187' <?php if($row['icono']==187) echo 'selected'; ?>>glyphicon-stat</option>
							<option data-icon="glyphicon glyphicon-sd-video" value='188' <?php if($row['icono']==188) echo 'selected'; ?>>glyphicon-sd-vide</option>
							<option data-icon="glyphicon glyphicon-hd-video" value='189' <?php if($row['icono']==189) echo 'selected'; ?>>glyphicon-hd-vide</option>
							<option data-icon="glyphicon glyphicon-subtitles" value='190' <?php if($row['icono']==190) echo 'selected'; ?>>glyphicon-subtitle</option>
							<option data-icon="glyphicon glyphicon-sound-stereo" value='191' <?php if($row['icono']==191) echo 'selected'; ?>>glyphicon-sound-stere</option>
							<option data-icon="glyphicon glyphicon-sound-dolby" value='192' <?php if($row['icono']==192) echo 'selected'; ?>>glyphicon-sound-dolb</option>
							<option data-icon="glyphicon glyphicon-sound-5-1" value='193' <?php if($row['icono']==193) echo 'selected'; ?>>glyphicon-sound-5-</option>
							<option data-icon="glyphicon glyphicon-sound-6-1" value='194' <?php if($row['icono']==194) echo 'selected'; ?>>glyphicon-sound-6-</option>
							<option data-icon="glyphicon glyphicon-sound-7-1" value='195' <?php if($row['icono']==195) echo 'selected'; ?>>glyphicon-sound-7-</option>
							<option data-icon="glyphicon glyphicon-copyright-mark" value='196' <?php if($row['icono']==196) echo 'selected'; ?>>glyphicon-copyright-mar</option>
							<option data-icon="glyphicon glyphicon-registration-mark" value='197' <?php if($row['icono']==197) echo 'selected'; ?>>glyphicon-registration-mar</option>
							<option data-icon="glyphicon glyphicon-cloud-download" value='198' <?php if($row['icono']==198) echo 'selected'; ?>>glyphicon-cloud-downloa</option>
							<option data-icon="glyphicon glyphicon-cloud-upload" value='199' <?php if($row['icono']==199) echo 'selected'; ?>>glyphicon-cloud-uploa</option>
							<option data-icon="glyphicon glyphicon-tree-conifer" value='200' <?php if($row['icono']==200) echo 'selected'; ?>>glyphicon-tree-conife</option>
							<option data-icon="glyphicon glyphicon-tree-deciduous" value='201' <?php if($row['icono']==201) echo 'selected'; ?>>glyphicon-tree-deciduou</option>
							<option data-icon="glyphicon glyphicon-cd" value='202' <?php if($row['icono']==202) echo 'selected'; ?>>glyphicon-c</option>
							<option data-icon="glyphicon glyphicon-save-file" value='203' <?php if($row['icono']==203) echo 'selected'; ?>>glyphicon-save-fil</option>
							<option data-icon="glyphicon glyphicon-open-file" value='204' <?php if($row['icono']==204) echo 'selected'; ?>>glyphicon-open-fil</option>
							<option data-icon="glyphicon glyphicon-level-up" value='205' <?php if($row['icono']==205) echo 'selected'; ?>>glyphicon-level-u</option>
							<option data-icon="glyphicon glyphicon-copy" value='206' <?php if($row['icono']==206) echo 'selected'; ?>>glyphicon-cop</option>
							<option data-icon="glyphicon glyphicon-paste" value='207' <?php if($row['icono']==207) echo 'selected'; ?>>glyphicon-past</option>
							<option data-icon="glyphicon glyphicon-alert" value='208' <?php if($row['icono']==208) echo 'selected'; ?>>glyphicon-aler</option>
							<option data-icon="glyphicon glyphicon-equalizer" value='209' <?php if($row['icono']==209) echo 'selected'; ?>>glyphicon-equalize</option>
							<option data-icon="glyphicon glyphicon-king" value='210' <?php if($row['icono']==210) echo 'selected'; ?>>glyphicon-kin</option>
							<option data-icon="glyphicon glyphicon-queen" value='211' <?php if($row['icono']==211) echo 'selected'; ?>>glyphicon-quee</option>
							<option data-icon="glyphicon glyphicon-pawn" value='212' <?php if($row['icono']==212) echo 'selected'; ?>>glyphicon-paw</option>
							<option data-icon="glyphicon glyphicon-bishop" value='213' <?php if($row['icono']==213) echo 'selected'; ?>>glyphicon-bisho</option>
							<option data-icon="glyphicon glyphicon-knight" value='214' <?php if($row['icono']==214) echo 'selected'; ?>>glyphicon-knigh</option>
							<option data-icon="glyphicon glyphicon-baby-formula" value='215' <?php if($row['icono']==215) echo 'selected'; ?>>glyphicon-baby-formul</option>
							<option data-icon="glyphicon glyphicon-tent" value='216' <?php if($row['icono']==216) echo 'selected'; ?>>glyphicon-ten</option>
							<option data-icon="glyphicon glyphicon-blackboard" value='217' <?php if($row['icono']==217) echo 'selected'; ?>>glyphicon-blackboar</option>
							<option data-icon="glyphicon glyphicon-bed" value='218' <?php if($row['icono']==218) echo 'selected'; ?>>glyphicon-be</option>
							<option data-icon="glyphicon glyphicon-apple" value='219' <?php if($row['icono']==219) echo 'selected'; ?>>glyphicon-appl</option>
							<option data-icon="glyphicon glyphicon-erase" value='220' <?php if($row['icono']==220) echo 'selected'; ?>>glyphicon-eras</option>
							<option data-icon="glyphicon glyphicon-hourglass" value='221' <?php if($row['icono']==221) echo 'selected'; ?>>glyphicon-hourglas</option>
							<option data-icon="glyphicon glyphicon-lamp" value='222' <?php if($row['icono']==222) echo 'selected'; ?>>glyphicon-lam</option>
							<option data-icon="glyphicon glyphicon-duplicate" value='223' <?php if($row['icono']==223) echo 'selected'; ?>>glyphicon-duplicat</option>
							<option data-icon="glyphicon glyphicon-piggy-bank" value='224' <?php if($row['icono']==224) echo 'selected'; ?>>glyphicon-piggy-ban</option>
							<option data-icon="glyphicon glyphicon-scissors" value='225' <?php if($row['icono']==225) echo 'selected'; ?>>glyphicon-scissor</option>
							<option data-icon="glyphicon glyphicon-bitcoin" value='226' <?php if($row['icono']==226) echo 'selected'; ?>>glyphicon-bitcoi</option>
							<option data-icon="glyphicon glyphicon-btc" value='227' <?php if($row['icono']==227) echo 'selected'; ?>>glyphicon-bt</option>
							<option data-icon="glyphicon glyphicon-xbt" value='228' <?php if($row['icono']==228) echo 'selected'; ?>>glyphicon-xb</option>
							<option data-icon="glyphicon glyphicon-yen" value='229' <?php if($row['icono']==229) echo 'selected'; ?>>glyphicon-ye</option>
							<option data-icon="glyphicon glyphicon-jpy" value='230' <?php if($row['icono']==230) echo 'selected'; ?>>glyphicon-jp</option>
							<option data-icon="glyphicon glyphicon-ruble" value='231' <?php if($row['icono']==231) echo 'selected'; ?>>glyphicon-rubl</option>
							<option data-icon="glyphicon glyphicon-rub" value='232' <?php if($row['icono']==232) echo 'selected'; ?>>glyphicon-ru</option>
							<option data-icon="glyphicon glyphicon-scale" value='233' <?php if($row['icono']==233) echo 'selected'; ?>>glyphicon-scal</option>
							<option data-icon="glyphicon glyphicon-ice-lolly" value='234' <?php if($row['icono']==234) echo 'selected'; ?>>glyphicon-ice-loll</option>
							<option data-icon="glyphicon glyphicon-ice-lolly-tasted" value='235' <?php if($row['icono']==235) echo 'selected'; ?>>glyphicon-ice-lolly-taste</option>
							<option data-icon="glyphicon glyphicon-education" value='236' <?php if($row['icono']==236) echo 'selected'; ?>>glyphicon-educatio</option>
							<option data-icon="glyphicon glyphicon-option-horizontal" value='237' <?php if($row['icono']==237) echo 'selected'; ?>>glyphicon-option-horizonta</option>
							<option data-icon="glyphicon glyphicon-option-vertical" value='238' <?php if($row['icono']==238) echo 'selected'; ?>>glyphicon-option-vertica</option>
							<option data-icon="glyphicon glyphicon-menu-hamburger" value='239' <?php if($row['icono']==239) echo 'selected'; ?>>glyphicon-menu-hamburge</option>
							<option data-icon="glyphicon glyphicon-modal-window" value='240' <?php if($row['icono']==240) echo 'selected'; ?>>glyphicon-modal-windo</option>
							<option data-icon="glyphicon glyphicon-oil" value='241' <?php if($row['icono']==241) echo 'selected'; ?>>glyphicon-oi</option>
							<option data-icon="glyphicon glyphicon-grain" value='242' <?php if($row['icono']==242) echo 'selected'; ?>>glyphicon-grai</option>
							<option data-icon="glyphicon glyphicon-sunglasses" value='243' <?php if($row['icono']==243) echo 'selected'; ?>>glyphicon-sunglasse</option>
							<option data-icon="glyphicon glyphicon-text-size" value='244' <?php if($row['icono']==244) echo 'selected'; ?>>glyphicon-text-siz</option>
							<option data-icon="glyphicon glyphicon-text-color" value='245' <?php if($row['icono']==245) echo 'selected'; ?>>glyphicon-text-colo</option>
							<option data-icon="glyphicon glyphicon-text-background" value='246' <?php if($row['icono']==246) echo 'selected'; ?>>glyphicon-text-backgroun</option>
							<option data-icon="glyphicon glyphicon-object-align-top" value='247' <?php if($row['icono']==247) echo 'selected'; ?>>glyphicon-object-align-to</option>
							<option data-icon="glyphicon glyphicon-object-align-bottom" value='248' <?php if($row['icono']==248) echo 'selected'; ?>>glyphicon-object-align-botto</option>
							<option data-icon="glyphicon glyphicon-object-align-horizontal" value='249' <?php if($row['icono']==249) echo 'selected'; ?>>glyphicon-object-align-horizonta</option>
							<option data-icon="glyphicon glyphicon-object-align-left" value='250' <?php if($row['icono']==250) echo 'selected'; ?>>glyphicon-object-align-lef</option>
							<option data-icon="glyphicon glyphicon-object-align-vertical" value='251' <?php if($row['icono']==251) echo 'selected'; ?>>glyphicon-object-align-vertica</option>
							<option data-icon="glyphicon glyphicon-object-align-right" value='252' <?php if($row['icono']==252) echo 'selected'; ?>>glyphicon-object-align-righ</option>
							<option data-icon="glyphicon glyphicon-triangle-right" value='253' <?php if($row['icono']==253) echo 'selected'; ?>>glyphicon-triangle-righ</option>
							<option data-icon="glyphicon glyphicon-triangle-left" value='254' <?php if($row['icono']==254) echo 'selected'; ?>>glyphicon-triangle-lef</option>
							<option data-icon="glyphicon glyphicon-triangle-bottom" value='255' <?php if($row['icono']==255) echo 'selected'; ?>>glyphicon-triangle-botto</option>
							<option data-icon="glyphicon glyphicon-triangle-top" value='256' <?php if($row['icono']==256) echo 'selected'; ?>>glyphicon-triangle-to</option>
							<option data-icon="glyphicon glyphicon-console" value='257' <?php if($row['icono']==257) echo 'selected'; ?>>glyphicon-consol</option>
							<option data-icon="glyphicon glyphicon-superscript" value='258' <?php if($row['icono']==258) echo 'selected'; ?>>glyphicon-superscrip</option>
							<option data-icon="glyphicon glyphicon-subscript" value='259' <?php if($row['icono']==259) echo 'selected'; ?>>glyphicon-subscrip</option>
							<option data-icon="glyphicon glyphicon-menu-left" value='260' <?php if($row['icono']==260) echo 'selected'; ?>>glyphicon-menu-lef</option>
							<option data-icon="glyphicon glyphicon-menu-right" value='261' <?php if($row['icono']==261) echo 'selected'; ?>>glyphicon-menu-righ</option>
							<option data-icon="glyphicon glyphicon-menu-down" value='262' <?php if($row['icono']==262) echo 'selected'; ?>>glyphicon-menu-dow</option>
							<option data-icon="glyphicon glyphicon-menu-up" value='263' <?php if($row['icono']==263) echo 'selected'; ?>>glyphicon-menu-u</option>
						</select> 
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-3 control-label">desplegable</label>
					<div class="col-sm-3">
						<select name="desplegable" class="form-control">
							<option value="">- Selecciona desplegable -</option>
							<option value="1"<?php if( $row['desplegable']==1) echo 'selected'; ?>>MODAL</option>
							<option value="2"<?php if( $row['desplegable']==2) echo 'selected'; ?>>PAGINA</option>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">tipo_tabla</label>
					<div class="col-sm-3">
						<select name="tipo_tabla" class="form-control">
							<option value="">- Selecciona tipo_tabla -</option>
							<option value="1" <?php if( $row['tipo_tabla']==1) echo 'selected'; ?>>PRINCIPAL</option>
							<option value="2" <?php if( $row['tipo_tabla']==2) echo 'selected'; ?>>SUBTABLA</option>
							<option value="3" <?php if( $row['tipo_tabla']==3) echo 'selected'; ?>>DOBLE LIGA</option>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">tabla_principal</label>
					<div class="col-sm-3">
						<select name="tabla_principal" class="form-control">
							<option value="">- Selecciona tabla_principal -</option>
			                <?php 
								$busquedatablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_sitio='$site';"); 
								while($_tablas=mysqli_fetch_assoc($busquedatablas))
								{
									if($row['tabla_principal']==$_tablas['id_tabla'])
									{
										echo '<option value="'.$_tablas['id_tabla'].'" selected>'.$_tablas['clave_tabla'].'</option>';
									}
									else
									{
										echo '<option value="'.$_tablas['id_tabla'].'">'.$_tablas['clave_tabla'].'</option>';
									}
								}
							?>
						</select> 
					</div>
                </div>
                
				<div class="form-group">
					<label class="col-sm-3 control-label">tabla_liga</label>
					<div class="col-sm-3">
						<select name="tabla_liga" class="form-control">
							<option value="">- Selecciona tabla_liga -</option>
			                <?php 
								$busquedatablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_sitio='$site';"); 
								while($_tablas=mysqli_fetch_assoc($busquedatablas))
								{
									if($row['tabla_liga']==$_tablas['id_tabla'])
									{
										echo '<option value="'.$_tablas['id_tabla'].'" selected>'.$_tablas['clave_tabla'].'</option>';
									}
									else
									{
										echo '<option value="'.$_tablas['id_tabla'].'">'.$_tablas['clave_tabla'].'</option>';
									}
								}
							?>
						</select> 
					</div>
                </div>
				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="TABLAS.php?SITE=<?php echo $site; ?>" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>