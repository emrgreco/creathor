-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2020 a las 08:04:31
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `creathor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campos`
--

CREATE TABLE `campos` (
  `id_campo` int(11) NOT NULL,
  `id_sitio` int(11) NOT NULL,
  `id_tabla` int(11) NOT NULL,
  `descripcion_campo` varchar(100) NOT NULL,
  `tipo_campo` int(11) NOT NULL,
  `longitud_campo` int(11) NOT NULL,
  `visible` int(11) NOT NULL,
  `editable` int(11) NOT NULL,
  `requerido` int(11) NOT NULL,
  `indice` int(11) NOT NULL DEFAULT 0,
  `busqueda` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `clave_campo` varchar(10) NOT NULL,
  `carpeta` varchar(255) NOT NULL,
  `jpeg` tinyint(4) NOT NULL,
  `png` tinyint(4) NOT NULL,
  `gif` tinyint(4) NOT NULL,
  `tif` tinyint(4) NOT NULL,
  `svg` tinyint(4) NOT NULL,
  `eps` tinyint(4) NOT NULL,
  `pdf` tinyint(4) NOT NULL,
  `xlsx` tinyint(4) NOT NULL,
  `xlsm` tinyint(4) NOT NULL,
  `doc` tinyint(4) NOT NULL,
  `docx` tinyint(4) NOT NULL,
  `dwg` tinyint(4) NOT NULL,
  `txt` tinyint(4) NOT NULL,
  `zip` tinyint(4) NOT NULL,
  `rar` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `campos`
--

INSERT INTO `campos` (`id_campo`, `id_sitio`, `id_tabla`, `descripcion_campo`, `tipo_campo`, `longitud_campo`, `visible`, `editable`, `requerido`, `indice`, `busqueda`, `create_at`, `update_at`, `clave_campo`, `carpeta`, `jpeg`, `png`, `gif`, `tif`, `svg`, `eps`, `pdf`, `xlsx`, `xlsm`, `doc`, `docx`, `dwg`, `txt`, `zip`, `rar`) VALUES
(512, 1, 2, 'AS,L', 1, 10, 1, 0, 1, 1, 0, '2019-12-12 13:16:57', '2019-12-12 13:16:57', 'ID', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(513, 1, 2, 'KAMSLAKM', 2, 10, 1, 1, 1, 0, 0, '2019-12-12 13:17:52', '2019-12-12 13:17:52', 'CLAVE', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(514, 1, 2, 'SALKMA', 8, 10, 1, 1, 0, 0, 0, '2019-12-12 13:18:27', '2019-12-12 13:18:27', 'PRECIO', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(515, 1, 2, 'ASAS', 2, 10, 1, 1, 0, 0, 1, '2019-12-12 13:21:05', '2019-12-12 13:21:05', 'UNIDAD', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(516, 1, 2, 'ARCHIVOS', 2, 20, 1, 1, 0, 0, 0, '2019-12-13 19:01:19', '2019-12-15 21:19:01', 'ARCHIVOS', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(517, 1, 3, 'id', 1, 10, 1, 0, 0, 1, 0, '2019-12-15 01:54:13', '2019-12-16 00:58:46', 'id', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(518, 1, 3, 'dos', 6, 30, 1, 1, 0, 0, 0, '2019-12-15 01:54:28', '2019-12-19 20:04:23', 'dos', 'uploads', 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(521, 1, 3, 'tres', 2, 10, 1, 1, 0, 0, 0, '2019-12-16 01:38:33', '2019-12-16 01:38:33', 'tres', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(522, 1, 3, 'OTRAIMAGEN', 6, 50, 1, 1, 0, 0, 0, '2019-12-18 20:54:28', '2019-12-19 20:04:33', 'OTRAIMAGEN', 'wakandas', 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0),
(523, 1, 3, 'OTRO', 7, 1, 1, 1, 0, 0, 0, '2019-12-19 02:11:35', '2019-12-19 02:11:35', 'OTRO', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(524, 1, 3, 'MASIMAGEN', 6, 100, 1, 1, 0, 0, 0, '2019-12-19 02:29:40', '2019-12-19 20:04:15', 'MASIMAGEN', 'OTRAMAS', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(525, 1, 2, 'PRUEBA', 6, 30, 1, 1, 0, 0, 0, '2019-12-19 16:37:39', '2019-12-19 17:33:48', 'PRUEBA', 'HOST', 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1),
(530, 1, 5, 'id', 1, 10, 0, 0, 0, 1, 0, '2019-12-22 22:34:07', '2019-12-22 22:34:07', 'id', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(531, 1, 5, 'dos', 2, 30, 1, 1, 1, 0, 1, '2019-12-22 22:39:48', '2019-12-22 22:41:15', 'dos', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sitio`
--

CREATE TABLE `sitio` (
  `id_sitio` int(11) NOT NULL,
  `clave_sitio` varchar(10) NOT NULL,
  `nombre_sitio` varchar(100) NOT NULL,
  `descripcion_sitio` varchar(1000) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sitio`
--

INSERT INTO `sitio` (`id_sitio`, `clave_sitio`, `nombre_sitio`, `descripcion_sitio`, `create_at`, `update_at`) VALUES
(1, 'OBRAS', 'prueba', 'a', '2019-01-14 21:21:38', '2019-12-15 18:58:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla`
--

CREATE TABLE `tabla` (
  `id_tabla` int(11) NOT NULL,
  `id_sitio` int(11) NOT NULL,
  `clave_tabla` varchar(10) NOT NULL,
  `descripcion_tabla` varchar(100) NOT NULL,
  `desplegable` int(11) NOT NULL,
  `icono` int(11) NOT NULL DEFAULT 0,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `tabla_principal` int(11) NOT NULL,
  `tabla_liga` tinyint(4) NOT NULL DEFAULT 0,
  `tipo_tabla` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tabla`
--

INSERT INTO `tabla` (`id_tabla`, `id_sitio`, `clave_tabla`, `descripcion_tabla`, `desplegable`, `icono`, `create_at`, `update_at`, `tabla_principal`, `tabla_liga`, `tipo_tabla`) VALUES
(2, 1, 'CONCEPTOS', 'SALJML', 1, 126, '2019-12-12 13:14:51', '2019-12-19 21:55:25', 0, 0, 1),
(3, 1, 'uno', 'uno', 2, 2, '2019-12-15 01:53:04', '2020-01-22 07:33:51', 5, 0, 2),
(5, 1, 'oais', 'oiasj', 1, 2, '2019-12-22 22:15:02', '2020-01-22 07:36:08', 2, 0, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campos`
--
ALTER TABLE `campos`
  ADD PRIMARY KEY (`id_campo`),
  ADD KEY `id_campo` (`id_campo`);

--
-- Indices de la tabla `sitio`
--
ALTER TABLE `sitio`
  ADD PRIMARY KEY (`id_sitio`),
  ADD KEY `id_sitio` (`id_sitio`);

--
-- Indices de la tabla `tabla`
--
ALTER TABLE `tabla`
  ADD PRIMARY KEY (`id_tabla`),
  ADD KEY `id_tabla` (`id_tabla`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `campos`
--
ALTER TABLE `campos`
  MODIFY `id_campo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=532;

--
-- AUTO_INCREMENT de la tabla `sitio`
--
ALTER TABLE `sitio`
  MODIFY `id_sitio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tabla`
--
ALTER TABLE `tabla`
  MODIFY `id_tabla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
