
		$(document).ready(function()
		{
			load(1);
			$( "#resultados" ).load( "ajax/nueva_estimacion.php" );
		});

		function load(page)
		{
			var q= $("#q").val();
			$("#loader").fadeIn('slow');
			$.ajax({
				url:'./ajax/conceptos_estimacion.php?action=ajax&page='+page+'&q='+q,
				 beforeSend: function(objeto)
				 {
				 $('#loader').html('<img src="./img/ajax-loader.gif"> Cargando...');
			  },
				success:function(data)
				{
					$(".outer_div").html(data).fadeIn('slow');
					$('#loader').html('');
					
				}
			});
		}

		function agregar (id)
		{
			var precio_final=document.getElementById('precio_final_'+id).value;
			var cantidad=document.getElementById('cantidad_'+id).value;
			//Inicia validacion
			if (isNaN(cantidad))
			{
			alert('Esto no es un numero');
			document.getElementById('cantidad_'+id).focus();
			return false;
			}
			if (isNaN(precio_final))
			{
			alert('Esto no es un numero');
			document.getElementById('precio_final_'+id).focus();
			return false;
			}
			//Fin validacion
			
			$.ajax({
		        type: "POST",
		        url: "./ajax/nueva_estimacion.php",
		        data: "id="+id+"&precio_final="+precio_final+"&cantidad="+cantidad,
				 beforeSend: function(objeto)
				 {
					$("#resultados").html("Mensaje: Cargando...");
				  },
		        success: function(datos)
		        {
				$("#resultados").html(datos);
				}
			});
		}
		
		function eliminar (id)
		{

			$.ajax({
				type: "GET",
				url: "./ajax/nueva_estimacion.php",
				data: "id="+id,
				beforeSend: function(objeto)
				{
					$("#resultados").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#resultados").html(datos);
				}
			});
		}
		
		
		$("#datos_guardar").submit(function(event)
		{
			var id_contrato = $("#id_contrato").val();

			if (id_contrato=="")
			{
				alert("Debes seleccionar un contrato");
				$("#contrato").focus();
				return false;
			}
			var parametros = $(this).serialize();//"id_contrato="+id_contrato;//
			console.log(parametros);
			$.ajax({
				type: "POST",
				url: "ajax/estimacion_nueva.php",
				data: parametros,
				beforeSend: function(objeto)
				{
					$("#resultados").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#resultados").html(datos);
				}
			});
			event.preventDefault();
		});
		
		$( "#guardar_contratista" ).submit(function( event ) {
		  $('#guardar_datos').attr("disabled", true);
		  
		 var parametros = $(this).serialize();
			 $.ajax({
					type: "POST",
					url: "ajax/contratista_nuevo.php",
					data: parametros,
					 beforeSend: function(objeto)
					 {
						$("#resultados_ajax").html("Mensaje: Cargando...");
					  },
					success: function(datos)
					{
					$("#resultados_ajax").html(datos);
					$('#guardar_datos').attr("disabled", false);
					load(1);
				  }
			});
		  event.preventDefault();
		});
		
		$( "#guardar_concepto" ).submit(function( event ) {
		  $('#guardar_datos').attr("disabled", true);
		  
		 var parametros = $(this).serialize();
			 $.ajax({
					type: "POST",
					url: "ajax/concepto_nuevo.php",
					data: parametros,
					 beforeSend: function(objeto)
					 {
						$("#resultados_ajax_conceptos").html("Mensaje: Cargando...");
					  },
					success: function(datos)
					{
					$("#resultados_ajax_conceptos").html(datos);
					$('#guardar_datos').attr("disabled", false);
					load(1);
				  }
			});
		  event.preventDefault();
		});

		function imprimir_estimacion(id_estimacion)
		{
			VentanaCentrada('./pdf/documentos/ver_estimacion.php?id_estimacion='+id_estimacion,'estimacion','','1024','768','true');
		}