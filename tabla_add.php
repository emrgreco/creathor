<?php
include("conexion.php");

if(!isset($_GET['SITE']))
{
	header("Location: SITIOS.php");
}
else
{
	$site=$_GET['SITE'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Creathor</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del campos &raquo; Agregar datos</h2>
			<hr />

			<?php
			if(isset($_POST['tabla_add']))
			{
				$clave_tabla		     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_tabla	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$desplegable= mysqli_real_escape_string($con,(strip_tags($_POST["desplegable"],ENT_QUOTES)));//Escanpando 
				$tabla_principal= mysqli_real_escape_string($con,(strip_tags($_POST["tabla_principal"],ENT_QUOTES)));//Escanpando caracteres 
				$tabla_liga= mysqli_real_escape_string($con,(strip_tags($_POST["tabla_liga"],ENT_QUOTES)));//Escanpando caracteres 
				$tipo_tabla= mysqli_real_escape_string($con,(strip_tags($_POST["tipo_tabla"],ENT_QUOTES)));//Escanpando caracteres 
				$icono= mysqli_real_escape_string($con,(strip_tags($_POST["icono"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at	 =  date("Y-m-d H:i:s"); 
				$create_at	 =  date("Y-m-d H:i:s"); 
			

				$cek = mysqli_query($con, "SELECT * FROM tabla WHERE clave_tabla='$clave_tabla'");
				if(mysqli_num_rows($cek) == 0)
				{
						$insert = mysqli_query($con, "INSERT INTO tabla(id_sitio, clave_tabla, descripcion_tabla, desplegable, icono, create_at, update_at, tabla_principal, tabla_liga, tipo_tabla) VALUES('$site', '$clave_tabla', '$descripcion_tabla','$desplegable', '$icono','$create_at', '$update_at', '$tabla_principal', '$tabla_liga', '$tipo_tabla')") or die(mysqli_error());
						if($insert){
							echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
							
							header("Location: TABLAS.php?SITE=$site");
						}else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
						}
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. Clave_tabla exite!</div>';
				}
			}
			?>

			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">Clave_tabla</label>
					<div class="col-sm-2">
						<input type="text" name="clave_tabla" class="form-control" placeholder="Clave_tabla" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_tabla</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_tabla" class="form-control" placeholder="descripcion_tabla" required>
					</div>
				</div>





				<div class="form-group">
					<label class="col-sm-3 control-label">icono</label>
					<div class="col-sm-3">
						<select name="icono" class="form-control selectpicker">
							<option value="">- Selecciona icono -</option>
							<option data-icon="glyphicon glyphicon-asterisk" value='1'>glyphicon-asterisk</option>
							<option data-icon="glyphicon glyphicon-plus" value='2'>glyphicon-plus</option>
							<option data-icon="glyphicon glyphicon-euro" value='3'>glyphicon-euro</option>
							<option data-icon="glyphicon glyphicon-eur" value='4'>glyphicon-eur</option>
							<option data-icon="glyphicon glyphicon-minus" value='5'>glyphicon-minus</option>
							<option data-icon="glyphicon glyphicon-cloud" value='6'>glyphicon-cloud</option>
							<option data-icon="glyphicon glyphicon-envelope" value='7'>glyphicon-envelope</option>
							<option data-icon="glyphicon glyphicon-pencil" value='8'>glyphicon-pencil</option>
							<option data-icon="glyphicon glyphicon-glass" value='9'>glyphicon-glass</option>
							<option data-icon="glyphicon glyphicon-music" value='10'>glyphicon-music</option>
							<option data-icon="glyphicon glyphicon-search" value='11'>glyphicon-search</option>
							<option data-icon="glyphicon glyphicon-heart" value='12'>glyphicon-heart</option>
							<option data-icon="glyphicon glyphicon-star" value='13'>glyphicon-star</option>
							<option data-icon="glyphicon glyphicon-star-empty" value='14'>glyphicon-star-empty</option>
							<option data-icon="glyphicon glyphicon-user" value='15'>glyphicon-user</option>
							<option data-icon="glyphicon glyphicon-film" value='16'>glyphicon-film</option>
							<option data-icon="glyphicon glyphicon-th-large" value='17'>glyphicon-th-large</option>
							<option data-icon="glyphicon glyphicon-th" value='18'>glyphicon-th</option>
							<option data-icon="glyphicon glyphicon-th-list" value='19'>glyphicon-th-list</option>
							<option data-icon="glyphicon glyphicon-ok" value='20'>glyphicon-ok</option>
							<option data-icon="glyphicon glyphicon-remove" value='21'>glyphicon-remove</option>
							<option data-icon="glyphicon glyphicon-zoom-in" value='22'>glyphicon-zoom-in</option>
							<option data-icon="glyphicon glyphicon-zoom-out" value='23'>glyphicon-zoom-out</option>
							<option data-icon="glyphicon glyphicon-off" value='24'>glyphicon-off</option>
							<option data-icon="glyphicon glyphicon-signal" value='25'>glyphicon-signal</option>
							<option data-icon="glyphicon glyphicon-cog" value='26'>glyphicon-cog</option>
							<option data-icon="glyphicon glyphicon-trash" value='27'>glyphicon-trash</option>
							<option data-icon="glyphicon glyphicon-home" value='28'>glyphicon-home</option>
							<option data-icon="glyphicon glyphicon-file" value='29'>glyphicon-file</option>
							<option data-icon="glyphicon glyphicon-time" value='30'>glyphicon-time</option>
							<option data-icon="glyphicon glyphicon-road" value='31'>glyphicon-road</option>
							<option data-icon="glyphicon glyphicon-download-alt" value='32'>glyphicon-download-alt</option>
							<option data-icon="glyphicon glyphicon-download" value='33'>glyphicon-download</option>
							<option data-icon="glyphicon glyphicon-upload" value='34'>glyphicon-upload</option>
							<option data-icon="glyphicon glyphicon-inbox" value='35'>glyphicon-inbox</option>
							<option data-icon="glyphicon glyphicon-play-circle" value='36'>glyphicon-play-circle</option>
							<option data-icon="glyphicon glyphicon-repeat" value='37'>glyphicon-repeat</option>
							<option data-icon="glyphicon glyphicon-refresh" value='38'>glyphicon-refresh</option>
							<option data-icon="glyphicon glyphicon-list-alt" value='39'>glyphicon-list-alt</option>
							<option data-icon="glyphicon glyphicon-lock" value='40'>glyphicon-lock</option>
							<option data-icon="glyphicon glyphicon-flag" value='41'>glyphicon-flag</option>
							<option data-icon="glyphicon glyphicon-headphones" value='42'>glyphicon-headphones</option>
							<option data-icon="glyphicon glyphicon-volume-off" value='43'>glyphicon-volume-off</option>
							<option data-icon="glyphicon glyphicon-volume-down" value='44'>glyphicon-volume-down</option>
							<option data-icon="glyphicon glyphicon-volume-up" value='45'>glyphicon-volume-up</option>
							<option data-icon="glyphicon glyphicon-qrcode" value='46'>glyphicon-qrcode</option>
							<option data-icon="glyphicon glyphicon-barcode" value='47'>glyphicon-barcode</option>
							<option data-icon="glyphicon glyphicon-tag" value='48'>glyphicon-tag</option>
							<option data-icon="glyphicon glyphicon-tags" value='49'>glyphicon-tags</option>
							<option data-icon="glyphicon glyphicon-book" value='50'>glyphicon-book</option>
							<option data-icon="glyphicon glyphicon-bookmark" value='51'>glyphicon-bookmark</option>
							<option data-icon="glyphicon glyphicon-print" value='52'>glyphicon-print</option>
							<option data-icon="glyphicon glyphicon-camera" value='53'>glyphicon-camera</option>
							<option data-icon="glyphicon glyphicon-font" value='54'>glyphicon-font</option>
							<option data-icon="glyphicon glyphicon-bold" value='55'>glyphicon-bold</option>
							<option data-icon="glyphicon glyphicon-italic" value='56'>glyphicon-italic</option>
							<option data-icon="glyphicon glyphicon-text-height" value='57'>glyphicon-text-height</option>
							<option data-icon="glyphicon glyphicon-text-width" value='58'>glyphicon-text-width</option>
							<option data-icon="glyphicon glyphicon-align-left" value='59'>glyphicon-align-left</option>
							<option data-icon="glyphicon glyphicon-align-center" value='60'>glyphicon-align-center</option>
							<option data-icon="glyphicon glyphicon-align-right" value='61'>glyphicon-align-right</option>
							<option data-icon="glyphicon glyphicon-align-justify" value='62'>glyphicon-align-justify</option>
							<option data-icon="glyphicon glyphicon-list" value='63'>glyphicon-list</option>
							<option data-icon="glyphicon glyphicon-indent-left" value='64'>glyphicon-indent-left</option>
							<option data-icon="glyphicon glyphicon-indent-right" value='65'>glyphicon-indent-right</option>
							<option data-icon="glyphicon glyphicon-facetime-video" value='66'>glyphicon-facetime-video</option>
							<option data-icon="glyphicon glyphicon-picture" value='67'>glyphicon-picture</option>
							<option data-icon="glyphicon glyphicon-map-marker" value='68'>glyphicon-map-marker</option>
							<option data-icon="glyphicon glyphicon-adjust" value='69'>glyphicon-adjust</option>
							<option data-icon="glyphicon glyphicon-tint" value='70'>glyphicon-tint</option>
							<option data-icon="glyphicon glyphicon-edit" value='71'>glyphicon-edit</option>
							<option data-icon="glyphicon glyphicon-share" value='72'>glyphicon-share</option>
							<option data-icon="glyphicon glyphicon-check" value='73'>glyphicon-check</option>
							<option data-icon="glyphicon glyphicon-move" value='74'>glyphicon-move</option>
							<option data-icon="glyphicon glyphicon-step-backward" value='75'>glyphicon-step-backward</option>
							<option data-icon="glyphicon glyphicon-fast-backward" value='76'>glyphicon-fast-backward</option>
							<option data-icon="glyphicon glyphicon-backward" value='77'>glyphicon-backward</option>
							<option data-icon="glyphicon glyphicon-play" value='78'>glyphicon-play</option>
							<option data-icon="glyphicon glyphicon-pause" value='79'>glyphicon-pause</option>
							<option data-icon="glyphicon glyphicon-stop" value='80'>glyphicon-stop</option>
							<option data-icon="glyphicon glyphicon-forward" value='81'>glyphicon-forward</option>
							<option data-icon="glyphicon glyphicon-fast-forward" value='82'>glyphicon-fast-forward</option>
							<option data-icon="glyphicon glyphicon-step-forward" value='83'>glyphicon-step-forward</option>
							<option data-icon="glyphicon glyphicon-eject" value='84'>glyphicon-eject</option>
							<option data-icon="glyphicon glyphicon-chevron-left" value='85'>glyphicon-chevron-left</option>
							<option data-icon="glyphicon glyphicon-chevron-right" value='86'>glyphicon-chevron-right</option>
							<option data-icon="glyphicon glyphicon-plus-sign" value='87'>glyphicon-plus-sign</option>
							<option data-icon="glyphicon glyphicon-minus-sign" value='88'>glyphicon-minus-sign</option>
							<option data-icon="glyphicon glyphicon-remove-sign" value='89'>glyphicon-remove-sign</option>
							<option data-icon="glyphicon glyphicon-ok-sign" value='90'>glyphicon-ok-sign</option>
							<option data-icon="glyphicon glyphicon-question-sign" value='91'>glyphicon-question-sign</option>
							<option data-icon="glyphicon glyphicon-info-sign" value='92'>glyphicon-info-sign</option>
							<option data-icon="glyphicon glyphicon-screenshot" value='93'>glyphicon-screenshot</option>
							<option data-icon="glyphicon glyphicon-remove-circle" value='94'>glyphicon-remove-circle</option>
							<option data-icon="glyphicon glyphicon-ok-circle" value='95'>glyphicon-ok-circle</option>
							<option data-icon="glyphicon glyphicon-ban-circle" value='96'>glyphicon-ban-circle</option>
							<option data-icon="glyphicon glyphicon-arrow-left" value='97'>glyphicon-arrow-left</option>
							<option data-icon="glyphicon glyphicon-arrow-right" value='98'>glyphicon-arrow-right</option>
							<option data-icon="glyphicon glyphicon-arrow-up" value='99'>glyphicon-arrow-up</option>
							<option data-icon="glyphicon glyphicon-arrow-down" value='100'>glyphicon-arrow-down</option>
							<option data-icon="glyphicon glyphicon-share-alt" value='101'>glyphicon-share-alt</option>
							<option data-icon="glyphicon glyphicon-resize-full" value='102'>glyphicon-resize-full</option>
							<option data-icon="glyphicon glyphicon-resize-small" value='103'>glyphicon-resize-small</option>
							<option data-icon="glyphicon glyphicon-exclamation-sign" value='104'>glyphicon-exclamation-sign</option>
							<option data-icon="glyphicon glyphicon-gift" value='105'>glyphicon-gift</option>
							<option data-icon="glyphicon glyphicon-leaf" value='106'>glyphicon-leaf</option>
							<option data-icon="glyphicon glyphicon-fire" value='107'>glyphicon-fire</option>
							<option data-icon="glyphicon glyphicon-eye-open" value='108'>glyphicon-eye-open</option>
							<option data-icon="glyphicon glyphicon-eye-close" value='109'>glyphicon-eye-close</option>
							<option data-icon="glyphicon glyphicon-warning-sign" value='110'>glyphicon-warning-sign</option>
							<option data-icon="glyphicon glyphicon-plane" value='111'>glyphicon-plane</option>
							<option data-icon="glyphicon glyphicon-calendar" value='112'>glyphicon-calendar</option>
							<option data-icon="glyphicon glyphicon-random" value='113'>glyphicon-rando</option>
							<option data-icon="glyphicon glyphicon-comment" value='114'>glyphicon-commen</option>
							<option data-icon="glyphicon glyphicon-magnet" value='115'>glyphicon-magne</option>
							<option data-icon="glyphicon glyphicon-chevron-up" value='116'>glyphicon-chevron-u</option>
							<option data-icon="glyphicon glyphicon-chevron-down" value='117'>glyphicon-chevron-dow</option>
							<option data-icon="glyphicon glyphicon-retweet" value='118'>glyphicon-retwee</option>
							<option data-icon="glyphicon glyphicon-shopping-cart" value='119'>glyphicon-shopping-car</option>
							<option data-icon="glyphicon glyphicon-folder-close" value='120'>glyphicon-folder-clos</option>
							<option data-icon="glyphicon glyphicon-folder-open" value='121'>glyphicon-folder-ope</option>
							<option data-icon="glyphicon glyphicon-resize-vertical" value='122'>glyphicon-resize-vertica</option>
							<option data-icon="glyphicon glyphicon-resize-horizontal" value='123'>glyphicon-resize-horizonta</option>
							<option data-icon="glyphicon glyphicon-hdd" value='124'>glyphicon-hd</option>
							<option data-icon="glyphicon glyphicon-bullhorn" value='125'>glyphicon-bullhor</option>
							<option data-icon="glyphicon glyphicon-bell" value='126'>glyphicon-bel</option>
							<option data-icon="glyphicon glyphicon-certificate" value='127'>glyphicon-certificat</option>
							<option data-icon="glyphicon glyphicon-thumbs-up" value='128'>glyphicon-thumbs-u</option>
							<option data-icon="glyphicon glyphicon-thumbs-down" value='129'>glyphicon-thumbs-dow</option>
							<option data-icon="glyphicon glyphicon-hand-right" value='130'>glyphicon-hand-righ</option>
							<option data-icon="glyphicon glyphicon-hand-left" value='131'>glyphicon-hand-lef</option>
							<option data-icon="glyphicon glyphicon-hand-up" value='132'>glyphicon-hand-u</option>
							<option data-icon="glyphicon glyphicon-hand-down" value='133'>glyphicon-hand-dow</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-right" value='134'>glyphicon-circle-arrow-righ</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-left" value='135'>glyphicon-circle-arrow-lef</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-up" value='136'>glyphicon-circle-arrow-u</option>
							<option data-icon="glyphicon glyphicon-circle-arrow-down" value='137'>glyphicon-circle-arrow-dow</option>
							<option data-icon="glyphicon glyphicon-globe" value='138'>glyphicon-glob</option>
							<option data-icon="glyphicon glyphicon-wrench" value='139'>glyphicon-wrenc</option>
							<option data-icon="glyphicon glyphicon-tasks" value='140'>glyphicon-task</option>
							<option data-icon="glyphicon glyphicon-filter" value='141'>glyphicon-filte</option>
							<option data-icon="glyphicon glyphicon-briefcase" value='142'>glyphicon-briefcas</option>
							<option data-icon="glyphicon glyphicon-fullscreen" value='143'>glyphicon-fullscree</option>
							<option data-icon="glyphicon glyphicon-dashboard" value='144'>glyphicon-dashboar</option>
							<option data-icon="glyphicon glyphicon-paperclip" value='145'>glyphicon-papercli</option>
							<option data-icon="glyphicon glyphicon-heart-empty" value='146'>glyphicon-heart-empt</option>
							<option data-icon="glyphicon glyphicon-link" value='147'>glyphicon-lin</option>
							<option data-icon="glyphicon glyphicon-phone" value='148'>glyphicon-phon</option>
							<option data-icon="glyphicon glyphicon-pushpin" value='149'>glyphicon-pushpi</option>
							<option data-icon="glyphicon glyphicon-usd" value='150'>glyphicon-us</option>
							<option data-icon="glyphicon glyphicon-gbp" value='151'>glyphicon-gb</option>
							<option data-icon="glyphicon glyphicon-sort" value='152'>glyphicon-sor</option>
							<option data-icon="glyphicon glyphicon-sort-by-alphabet" value='153'>glyphicon-sort-by-alphabe</option>
							<option data-icon="glyphicon glyphicon-sort-by-alphabet-alt" value='154'>glyphicon-sort-by-alphabet-al</option>
							<option data-icon="glyphicon glyphicon-sort-by-order" value='155'>glyphicon-sort-by-orde</option>
							<option data-icon="glyphicon glyphicon-sort-by-order-alt" value='156'>glyphicon-sort-by-order-al</option>
							<option data-icon="glyphicon glyphicon-sort-by-attributes" value='157'>glyphicon-sort-by-attribute</option>
							<option data-icon="glyphicon glyphicon-sort-by-attributes-alt" value='158'>glyphicon-sort-by-attributes-al</option>
							<option data-icon="glyphicon glyphicon-unchecked" value='159'>glyphicon-unchecke</option>
							<option data-icon="glyphicon glyphicon-expand" value='160'>glyphicon-expan</option>
							<option data-icon="glyphicon glyphicon-collapse-down" value='161'>glyphicon-collapse-dow</option>
							<option data-icon="glyphicon glyphicon-collapse-up" value='162'>glyphicon-collapse-u</option>
							<option data-icon="glyphicon glyphicon-log-in" value='163'>glyphicon-log-i</option>
							<option data-icon="glyphicon glyphicon-flash" value='164'>glyphicon-flas</option>
							<option data-icon="glyphicon glyphicon-log-out" value='165'>glyphicon-log-ou</option>
							<option data-icon="glyphicon glyphicon-new-window" value='166'>glyphicon-new-windo</option>
							<option data-icon="glyphicon glyphicon-record" value='167'>glyphicon-recor</option>
							<option data-icon="glyphicon glyphicon-save" value='168'>glyphicon-sav</option>
							<option data-icon="glyphicon glyphicon-open" value='169'>glyphicon-ope</option>
							<option data-icon="glyphicon glyphicon-saved" value='170'>glyphicon-save</option>
							<option data-icon="glyphicon glyphicon-import" value='171'>glyphicon-impor</option>
							<option data-icon="glyphicon glyphicon-export" value='172'>glyphicon-expor</option>
							<option data-icon="glyphicon glyphicon-send" value='173'>glyphicon-sen</option>
							<option data-icon="glyphicon glyphicon-floppy-disk" value='174'>glyphicon-floppy-dis</option>
							<option data-icon="glyphicon glyphicon-floppy-saved" value='175'>glyphicon-floppy-save</option>
							<option data-icon="glyphicon glyphicon-floppy-remove" value='176'>glyphicon-floppy-remov</option>
							<option data-icon="glyphicon glyphicon-floppy-save" value='177'>glyphicon-floppy-sav</option>
							<option data-icon="glyphicon glyphicon-floppy-open" value='178'>glyphicon-floppy-ope</option>
							<option data-icon="glyphicon glyphicon-credit-card" value='179'>glyphicon-credit-car</option>
							<option data-icon="glyphicon glyphicon-transfer" value='180'>glyphicon-transfe</option>
							<option data-icon="glyphicon glyphicon-cutlery" value='181'>glyphicon-cutler</option>
							<option data-icon="glyphicon glyphicon-header" value='182'>glyphicon-heade</option>
							<option data-icon="glyphicon glyphicon-compressed" value='183'>glyphicon-compresse</option>
							<option data-icon="glyphicon glyphicon-earphone" value='184'>glyphicon-earphon</option>
							<option data-icon="glyphicon glyphicon-phone-alt" value='185'>glyphicon-phone-al</option>
							<option data-icon="glyphicon glyphicon-tower" value='186'>glyphicon-towe</option>
							<option data-icon="glyphicon glyphicon-stats" value='187'>glyphicon-stat</option>
							<option data-icon="glyphicon glyphicon-sd-video" value='188'>glyphicon-sd-vide</option>
							<option data-icon="glyphicon glyphicon-hd-video" value='189'>glyphicon-hd-vide</option>
							<option data-icon="glyphicon glyphicon-subtitles" value='190'>glyphicon-subtitle</option>
							<option data-icon="glyphicon glyphicon-sound-stereo" value='191'>glyphicon-sound-stere</option>
							<option data-icon="glyphicon glyphicon-sound-dolby" value='192'>glyphicon-sound-dolb</option>
							<option data-icon="glyphicon glyphicon-sound-5-1" value='193'>glyphicon-sound-5-</option>
							<option data-icon="glyphicon glyphicon-sound-6-1" value='194'>glyphicon-sound-6-</option>
							<option data-icon="glyphicon glyphicon-sound-7-1" value='195'>glyphicon-sound-7-</option>
							<option data-icon="glyphicon glyphicon-copyright-mark" value='196'>glyphicon-copyright-mar</option>
							<option data-icon="glyphicon glyphicon-registration-mark" value='197'>glyphicon-registration-mar</option>
							<option data-icon="glyphicon glyphicon-cloud-download" value='198'>glyphicon-cloud-downloa</option>
							<option data-icon="glyphicon glyphicon-cloud-upload" value='199'>glyphicon-cloud-uploa</option>
							<option data-icon="glyphicon glyphicon-tree-conifer" value='200'>glyphicon-tree-conife</option>
							<option data-icon="glyphicon glyphicon-tree-deciduous" value='201'>glyphicon-tree-deciduou</option>
							<option data-icon="glyphicon glyphicon-cd" value='202'>glyphicon-c</option>
							<option data-icon="glyphicon glyphicon-save-file" value='203'>glyphicon-save-fil</option>
							<option data-icon="glyphicon glyphicon-open-file" value='204'>glyphicon-open-fil</option>
							<option data-icon="glyphicon glyphicon-level-up" value='205'>glyphicon-level-u</option>
							<option data-icon="glyphicon glyphicon-copy" value='206'>glyphicon-cop</option>
							<option data-icon="glyphicon glyphicon-paste" value='207'>glyphicon-past</option>
							<option data-icon="glyphicon glyphicon-alert" value='208'>glyphicon-aler</option>
							<option data-icon="glyphicon glyphicon-equalizer" value='209'>glyphicon-equalize</option>
							<option data-icon="glyphicon glyphicon-king" value='210'>glyphicon-kin</option>
							<option data-icon="glyphicon glyphicon-queen" value='211'>glyphicon-quee</option>
							<option data-icon="glyphicon glyphicon-pawn" value='212'>glyphicon-paw</option>
							<option data-icon="glyphicon glyphicon-bishop" value='213'>glyphicon-bisho</option>
							<option data-icon="glyphicon glyphicon-knight" value='214'>glyphicon-knigh</option>
							<option data-icon="glyphicon glyphicon-baby-formula" value='215'>glyphicon-baby-formul</option>
							<option data-icon="glyphicon glyphicon-tent" value='216'>glyphicon-ten</option>
							<option data-icon="glyphicon glyphicon-blackboard" value='217'>glyphicon-blackboar</option>
							<option data-icon="glyphicon glyphicon-bed" value='218'>glyphicon-be</option>
							<option data-icon="glyphicon glyphicon-apple" value='219'>glyphicon-appl</option>
							<option data-icon="glyphicon glyphicon-erase" value='220'>glyphicon-eras</option>
							<option data-icon="glyphicon glyphicon-hourglass" value='221'>glyphicon-hourglas</option>
							<option data-icon="glyphicon glyphicon-lamp" value='222'>glyphicon-lam</option>
							<option data-icon="glyphicon glyphicon-duplicate" value='223'>glyphicon-duplicat</option>
							<option data-icon="glyphicon glyphicon-piggy-bank" value='224'>glyphicon-piggy-ban</option>
							<option data-icon="glyphicon glyphicon-scissors" value='225'>glyphicon-scissor</option>
							<option data-icon="glyphicon glyphicon-bitcoin" value='226'>glyphicon-bitcoi</option>
							<option data-icon="glyphicon glyphicon-btc" value='227'>glyphicon-bt</option>
							<option data-icon="glyphicon glyphicon-xbt" value='228'>glyphicon-xb</option>
							<option data-icon="glyphicon glyphicon-yen" value='229'>glyphicon-ye</option>
							<option data-icon="glyphicon glyphicon-jpy" value='230'>glyphicon-jp</option>
							<option data-icon="glyphicon glyphicon-ruble" value='231'>glyphicon-rubl</option>
							<option data-icon="glyphicon glyphicon-rub" value='232'>glyphicon-ru</option>
							<option data-icon="glyphicon glyphicon-scale" value='233'>glyphicon-scal</option>
							<option data-icon="glyphicon glyphicon-ice-lolly" value='234'>glyphicon-ice-loll</option>
							<option data-icon="glyphicon glyphicon-ice-lolly-tasted" value='235'>glyphicon-ice-lolly-taste</option>
							<option data-icon="glyphicon glyphicon-education" value='236'>glyphicon-educatio</option>
							<option data-icon="glyphicon glyphicon-option-horizontal" value='237'>glyphicon-option-horizonta</option>
							<option data-icon="glyphicon glyphicon-option-vertical" value='238'>glyphicon-option-vertica</option>
							<option data-icon="glyphicon glyphicon-menu-hamburger" value='239'>glyphicon-menu-hamburge</option>
							<option data-icon="glyphicon glyphicon-modal-window" value='240'>glyphicon-modal-windo</option>
							<option data-icon="glyphicon glyphicon-oil" value='241'>glyphicon-oi</option>
							<option data-icon="glyphicon glyphicon-grain" value='242'>glyphicon-grai</option>
							<option data-icon="glyphicon glyphicon-sunglasses" value='243'>glyphicon-sunglasse</option>
							<option data-icon="glyphicon glyphicon-text-size" value='244'>glyphicon-text-siz</option>
							<option data-icon="glyphicon glyphicon-text-color" value='245'>glyphicon-text-colo</option>
							<option data-icon="glyphicon glyphicon-text-background" value='246'>glyphicon-text-backgroun</option>
							<option data-icon="glyphicon glyphicon-object-align-top" value='247'>glyphicon-object-align-to</option>
							<option data-icon="glyphicon glyphicon-object-align-bottom" value='248'>glyphicon-object-align-botto</option>
							<option data-icon="glyphicon glyphicon-object-align-horizontal" value='249'>glyphicon-object-align-horizonta</option>
							<option data-icon="glyphicon glyphicon-object-align-left" value='250'>glyphicon-object-align-lef</option>
							<option data-icon="glyphicon glyphicon-object-align-vertical" value='251'>glyphicon-object-align-vertica</option>
							<option data-icon="glyphicon glyphicon-object-align-right" value='252'>glyphicon-object-align-righ</option>
							<option data-icon="glyphicon glyphicon-triangle-right" value='253'>glyphicon-triangle-righ</option>
							<option data-icon="glyphicon glyphicon-triangle-left" value='254'>glyphicon-triangle-lef</option>
							<option data-icon="glyphicon glyphicon-triangle-bottom" value='255'>glyphicon-triangle-botto</option>
							<option data-icon="glyphicon glyphicon-triangle-top" value='256'>glyphicon-triangle-to</option>
							<option data-icon="glyphicon glyphicon-console" value='257'>glyphicon-consol</option>
							<option data-icon="glyphicon glyphicon-superscript" value='258'>glyphicon-superscrip</option>
							<option data-icon="glyphicon glyphicon-subscript" value='259'>glyphicon-subscrip</option>
							<option data-icon="glyphicon glyphicon-menu-left" value='260'>glyphicon-menu-lef</option>
							<option data-icon="glyphicon glyphicon-menu-right" value='261'>glyphicon-menu-righ</option>
							<option data-icon="glyphicon glyphicon-menu-down" value='262'>glyphicon-menu-dow</option>
							<option data-icon="glyphicon glyphicon-menu-up" value='263'>glyphicon-menu-u</option>
						</select> 
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">desplegable</label>
					<div class="col-sm-3">
						<select name="desplegable" class="form-control">
							<option value="">- Selecciona desplegable -</option>
							<option value="1">MODAL</option>
							<option value="2">PAGINA</option>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">tipo_tabla</label>
					<div class="col-sm-3">
						<select name="tipo_tabla" class="form-control">
							<option value="">- Selecciona tipo_tabla -</option>
							<option value="1">PRINCIPAL</option>
							<option value="2">SUBTABLA</option>
							<option value="3">DOBLE LIGA</option>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">tabla_principal</label>
					<div class="col-sm-3">
						<select name="tabla_principal" class="form-control">
							<option value="">- Selecciona tabla_principal -</option>
			                <?php 
			                echo "as";
								$busquedatablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_sitio='$site' AND tabla_principal=0;"); 
								while($_tablas=mysqli_fetch_assoc($busquedatablas))
								{
									echo '<option value="'.$_tablas['id_tabla'].'">'.$_tablas['clave_tabla'].'</option>';
								}
							?>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">tabla_liga</label>
					<div class="col-sm-3">
						<select name="tabla_liga" class="form-control">
							<option value="">- Selecciona tabla_liga -</option>
			                <?php 
			                echo "as";
								$busquedatablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_sitio='$site' AND tabla_liga=0;"); 
								while($_tablas=mysqli_fetch_assoc($busquedatablas))
								{
									echo '<option value="'.$_tablas['id_tabla'].'">'.$_tablas['clave_tabla'].'</option>';
								}
							?>
						</select> 
					</div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="tabla_add" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="TABLAS.php?SITE=<?php ECHO $site; ?>" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>

			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>
