<?php 
 
	require_once('conexion.php');
	function script_pagina($url, $id_tabla)
	{
		global $con;
		$sql1 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$id_tabla'");
		$row3 = mysqli_fetch_assoc($sql1);

		$out = fopen($url.'/'.$row3['clave_tabla'].".js", "w+");
		fwrite($out,'		$(document).ready(function(){ $("#resultados").fadeOut(2000);load(1);});'.PHP_EOL);

		fwrite($out,'		
		function load(page)
		{
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
					url:"../actions/'.$row3['clave_tabla'].'_search.php?action=ajax&page="+page+"&q="+q,
					beforeSend: function(objeto)
				{
					$("#loader").html("'. "<img src='../img/ajax-loader.gif'> Cargando..." .'");
				},
				success:function(data)
				{
					$(".outer_div").html(data).fadeIn("slow");
					$("#loader").html("");					
				}
			});
		}'.PHP_EOL);

			
				
		fwrite($out,'		
		function eliminar (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el '.$row3['clave_tabla'].'"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/'.$row3['clave_tabla'].'_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultados").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultados").html(datos);
						load(1);
					}
				});
			}
		}'.PHP_EOL);
				
				
		fclose($out);

	}