<?php

	require_once('conexion.php');
	

function msqli_db_crear_conexion($url,$id_sitio)
{
	global $con;

	global $db_host;
	global $db_user;
	global $db_pass;


	$tablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_sitio='$id_sitio'");
	$SITIO = mysqli_query($con, "SELECT * FROM SITIO WHERE id_sitio='$id_sitio'");
	$DBASE=mysqli_fetch_assoc($SITIO);
	$out = fopen($url.'/db.sql', "w+");		
	$header='
		DROP DATABASE IF EXISTS '.$DBASE['clave_sitio'].';
		CREATE DATABASE IF NOT EXISTS '.$DBASE['clave_sitio'].';
		SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
		SET time_zone = "+00:00"; ';
	$consulta='';
	$cadena='';
	$indice=array('');
	$indice_consulta='';

	while( $tablas_row = mysqli_fetch_assoc($tablas))
	{
		$tabla=$tablas_row['id_tabla'];
		$campos = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$tabla'");
		$cadena.='

		CREATE TABLE '.$tablas_row['clave_tabla'].' (';
		while( $campos_row = mysqli_fetch_assoc($campos))
		{	
			$cadena.="`".$campos_row['clave_campo']."` ";
			switch (intval($campos_row['tipo_campo']))
			{

				case 1://INT
					$cadena.='INT('.intval($campos_row['longitud_campo']).')';if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					if($campos_row['indice']==1)
					{

						$indice_consulta.='
						ALTER TABLE `'.$tablas_row['clave_tabla'].'` ADD PRIMARY KEY (`'.$campos_row['clave_campo'].'`), ADD KEY `'.$campos_row['clave_campo'].'` (`'.$campos_row['clave_campo'].'`);
						ALTER TABLE `'.$tablas_row['clave_tabla'].'` MODIFY `'.$campos_row['clave_campo'].'` int('.$campos_row['longitud_campo'].')  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;';
					}
					break;

				case 2://VARCHAR
					$cadena.='VARCHAR('.intval($campos_row['longitud_campo']).")";if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;

				case 3://DATE
					$cadena.='DATE ';if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					$ndate=1;
					break;

				case 4://DATETIME
					$cadena.='DATETIME ';if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					$ndate=1;
					break;

				case 5://TEXT
					$cadena.='VARCHAR('.intval($campos_row['longitud_campo']).")";if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;

				case 6://FILE
					$cadena.='VARCHAR('.intval($campos_row['longitud_campo']).")";if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;

				case 7://BOOLEAN
					$cadena.='TINYINT(1)';if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;

				case 8://DOUBLE
					$cadena.='DOUBLE ';if($campos_row['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;

				default:
					# code...
					break;
			}
		}
		

		if ($tablas_row['tabla_principal']!=0)
		{
			$tabla_principal = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$tablas_row["tabla_principal"]."';");
			$row10 = mysqli_fetch_assoc($tabla_principal);
			$campos_tabla_principal = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='".$tablas_row["tabla_principal"]."';");
			while($row9 = mysqli_fetch_assoc($campos_tabla_principal))
			{
				if($row9['indice']==1)
				{
					$cadena.="`".$row9['clave_campo']."_".$row10['clave_tabla']."` ";
					$cadena.='INT('.intval($row9['longitud_campo']).')';if($row9['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
					break;
				}
			}
		}

		$cadena=substr($cadena, 0, -2).") ENGINE=InnoDB DEFAULT CHARSET=latin1;".$indice_consulta;
		$indice=array('');

		$consulta.=$cadena;
		$cadena='';
		$indice_consulta='';
	}
		fwrite($out, $header.$consulta.PHP_EOL);

		
	fclose($out);
}