<?php 
 
	require_once('conexion.php');
	function script_modal($url, $id_tabla)
	{
		global $con;
		$sqltablas = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$id_tabla'");
		$tabla_row = mysqli_fetch_assoc($sqltablas);



		$sqlcampos = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
		$sqlcampos2 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");

		$out = fopen($url.'/'.$tabla_row['clave_tabla'].".js", "w+");
		fwrite($out,'		
		$(document).ready(function(){
			$("#resultados").fadeOut(2000);load(1);
		});'.PHP_EOL);

		fwrite($out,'		
		function load(page)
		{
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
					url:"../actions/'.$tabla_row['clave_tabla'].'_search.php?action=ajax&page="+page+"&q="+q,
					beforeSend: function(objeto)
				{
					$("#loader").html("'. "<img src='../img/ajax-loader.gif'> Cargando..." .'");
				},
				success:function(data)
				{
					$(".outer_div").html(data).fadeIn("slow");
					$("#loader").html("");
					borrar_modales();
					$("#'.$tabla_row['clave_tabla'].'_modal_add").removeData();

					
					//$("#'.$tabla_row['clave_tabla'].'_modal_add" ).modal("hide");
					//$("#'.$tabla_row['clave_tabla'].'_modal_edit" ).modal("hide");


					
				}
			});
		}'.PHP_EOL);

			
				
		fwrite($out,'		
		function eliminar (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el '.$tabla_row['clave_tabla'].'"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/'.$tabla_row['clave_tabla'].'_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultados").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultados").html(datos);
						load(1);
					}
				});
			}
		}'.PHP_EOL);
				
				
		
		fwrite($out,'		
		$("#'.$tabla_row['clave_tabla'].'_add_form" ).submit(function( event ) {
			$("#'.$tabla_row['clave_tabla'].'_guardar_datos").attr("disabled", true);
		  
		 	var parametros = $(this).serialize();
			 $.ajax({
					type: "POST",
					url: "../actions/'.$tabla_row['clave_tabla'].'_add_action.php",
					data: parametros,
					beforeSend: function(objeto)
					{
						$("#'.$tabla_row['clave_tabla'].'_resultados_modal_add").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#'.$tabla_row['clave_tabla'].'_resultados_modal_add").html(datos);
						$("#'.$tabla_row['clave_tabla'].'_guardar_datos").attr("disabled", false);
						load(1);
				  	}
			});
			event.preventDefault();
		});'.PHP_EOL);


		fwrite($out, '
		$("#'.$tabla_row['clave_tabla'].'_edit_form" ).submit(function( event ) {
			$("#'.$tabla_row['clave_tabla'].'_actualizar_datos").attr("disabled", true);
			var parametros = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "../actions/'.$tabla_row['clave_tabla'].'_edit_action.php",
				data: parametros,
				beforeSend: function(objeto)
				{
					$("#'.$tabla_row['clave_tabla'].'_resultados_modal_edit").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#'.$tabla_row['clave_tabla'].'_resultados_modal_edit").html(datos);
					load(1);
				}
			});
			event.preventDefault();
		});'.PHP_EOL);
		

		fwrite($out,'
		function obtener_datos(id)
		{
		');


		while($row2 = mysqli_fetch_assoc($sqlcampos))
		{
			if($row2['indice']==1)
			{
				fwrite($out,'$("#modz_'.$row2['clave_campo'].'").val("");');
				fwrite($out,'$("#modz_'.$row2['clave_campo'].'").val(id);');

				if ($tabla_row['tabla_principal']!=0)
				{
					$tabla_principal = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$tabla_row["tabla_principal"]."';");
					$row10 = mysqli_fetch_assoc($tabla_principal);
					$campos_tabla_principal = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='".$tabla_row["tabla_principal"]."';");
					while($row9 = mysqli_fetch_assoc($campos_tabla_principal))
					{
						if($row9['indice']==1)
						{
							fwrite($out,'$("#modz_'.$row9['clave_campo'].'_'.$tabla_row['clave_tabla'].'").val("");');
							fwrite($out,'$("#modz_'.$row9['clave_campo'].'_'.$tabla_row['clave_tabla'].'").val(id);');
							break;
						}
					}
				}

			}
			if($row2['editable']==1)
			{
				fwrite($out,'
				$("#modz_'.$row2['clave_campo'].'").val("");
				var '.$row2['clave_campo'].' = $("#mod_'.$row2['clave_campo'].'"+id).val();
				$("#modz_'.$row2['clave_campo'].'").val('.$row2['clave_campo'].');
				');
			}
		}
		fwrite($out,'
		}');



		fwrite($out,'
		function borrar_modales()
		{');

		while($row2 = mysqli_fetch_assoc($sqlcampos2))
		{
			fwrite($out,'
			$("#modz_'.$row2['clave_campo'].'").val("");
			$("#mod_'.$row2['clave_campo'].'").val("");');
			if ($tabla_row['tabla_principal']!=0)
			{
				$tabla_principal = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$tabla_row["tabla_principal"]."';");
				$row10 = mysqli_fetch_assoc($tabla_principal);
				$campos_tabla_principal = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='".$tabla_row["tabla_principal"]."';");
				while($row9 = mysqli_fetch_assoc($campos_tabla_principal))
				{
					if($row9['indice']==1)
					{
						fwrite($out,'
						$("#modz_'.$row9['clave_campo'].'_'.$tabla_row['clave_tabla'].'").val("");
						$("#mod_'.$row9['clave_campo'].'_'.$tabla_row['clave_tabla'].'").val("");');
						break;
					}
				}
			}
		}

		fwrite($out,'
		}');







		fclose($out);

	}