<?php 

	require_once('conexion.php');
	function action_edit($url, $id_tabla)
	{
		global $con;

		$sql1 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
		$sql2 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$id_tabla'");
		$row3 = mysqli_fetch_assoc($sql2);

		$out = fopen($url.'/'.$row3['clave_tabla']."_edit_action.php", "w+");
		fwrite($out,'<?php '.PHP_EOL);


/*
fwrite($out,'echo "<pre>"; '.PHP_EOL);
fwrite($out,'var_dump($_POST); '.PHP_EOL);
fwrite($out,'echo "</pre>"; '.PHP_EOL);
*/
		$editable=0;
		while($row2 = mysqli_fetch_assoc($sql1))
		{

			if($row2['indice']==1||$row2['requerido']==1)
			{
				fwrite($out,'if (empty($_POST["modz_'.$row2['clave_campo'].'"])){ $errors[] = "Error en modz_'.$row2['clave_campo'].'"; }'.PHP_EOL);
			}
			if( $row2['editable']==1 && $row2['requerido']==1 )
			{
				$editable=$editable+1;
			}
		}
		
		
		$sql1 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
		$i=0;
		fwrite($out,'if('.PHP_EOL);
		while($row2 = mysqli_fetch_assoc($sql1))
		{
			if($row2['editable']==1 && $row2['requerido']==1)
			{
				if($i==0)
				{
					fwrite($out,'!empty($_POST["modz_'.$row2['clave_campo'].'"]) '.PHP_EOL);
				}
				else
				{
					fwrite($out,'&& !empty($_POST["modz_'.$row2['clave_campo'].'"]) '.PHP_EOL);
				}

				$i=$i+1;
			}
		}
		if($i==0)
			fwrite($out,' 1==1 '.PHP_EOL);

		fwrite($out,')'.PHP_EOL);
		fwrite($out,'{'.PHP_EOL);
		fwrite($out,'require_once ("../config/db.php");'.PHP_EOL);
		fwrite($out,'require_once ("../config/conexion.php");'.PHP_EOL);


		
		$sql1 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
		$i=0;
		$consulta='$sql="UPDATE '.$row3['clave_tabla'].' SET ';
		while($row2 = mysqli_fetch_assoc($sql1))
		{

			if($row2['indice']==1)
			{
				fwrite($out, '$'.$row2['clave_campo'].'=mysqli_real_escape_string($con,(strip_tags($_POST["modz_'.$row2['clave_campo'].'"],ENT_QUOTES)));'.PHP_EOL);
			}
			if($row2['editable']==1 && $row2['indice']!=1 )
			{
				fwrite($out, '$'.$row2['clave_campo'].'=mysqli_real_escape_string($con,(strip_tags($_POST["modz_'.$row2['clave_campo'].'"],ENT_QUOTES)));'.PHP_EOL);
				if($i<=$editable)
				{
					$consulta.=$row2['clave_campo']."='$".$row2['clave_campo']."', ";
					$i=$i+1;
				}
				else if($i>$editable)
				{
					$consulta.=$row2['clave_campo']."='$".$row2['clave_campo']."', ";
				}
			}
		}
		$consulta=substr($consulta, 0, -2);
		
		$sql1 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
		$i=0;
		while($row2 = mysqli_fetch_assoc($sql1))
		{
			if($row2['indice']==1 && $i==0)
			{
				$consulta.=' WHERE '.$row2['clave_campo']." = '$".$row2['clave_campo']."'".'';
				$i=$i+1;
			}
			else if($row2['indice']==1 )
			{
				$consulta.=' AND '.$row2['clave_campo']." = '$".$row2['clave_campo']."'".'';
				$i=$i+1;
			}
		}
		fwrite($out, $consulta.'";'.PHP_EOL);
		fwrite($out,'$query_update = mysqli_query($con,$sql);'.PHP_EOL);
		fwrite($out, '
				if ($query_update)
				{
					$messages[] = "concepto ha sido actualizado satisfactoriamente.";
				} else{
					$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
				}
			} else {
				$errors []= "Error desconocido.";
			}
			
			if (isset($errors))
			{
				
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Error!</strong> 
						<?php
							foreach ($errors as $error)
							{
								echo $error;
							}
							?>
				</div>
				<?php
				}
				if (isset($messages))
				{
					
					?>
					<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>¡Bien hecho!</strong>
							<?php
								foreach ($messages as $message)
								{
									echo $message;
								}
								?>
					</div>'.PHP_EOL);
			fwrite($out, '<?php } ?>'.PHP_EOL);
		}