

<?php 
	//$id_tabla=$_GET['TABLA'];

	require_once('conexion.php');
	

function modal_add($url,$id_tabla)
{
	global $con; 
	$sql1 = mysqli_query($con, "SELECT * FROM campos WHERE id_tabla='$id_tabla'");
	$sql2 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$id_tabla'");
	$row3 = mysqli_fetch_assoc($sql2);
	$out = fopen($url.'/'.$row3['clave_tabla']."_add.php", "w+");
	fwrite($out, '<div class="modal fade" id="'.$row3['clave_tabla'].'_modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="'.$row3['clave_tabla'].'_modal_addLabel"><i class="');
								fwrite($out, icono($row3['icono']));

					fwrite($out,'"></i> NUEVO '.$row3['clave_tabla'].'</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" method="post" id="'.$row3['clave_tabla'].'_add_form" name="'.$row3['clave_tabla'].'_add_form">
						<div id="'.$row3['clave_tabla'].'_resultados_modal_add"></div>'.PHP_EOL);

						while($row = mysqli_fetch_assoc($sql1))
						{ 
							if( $row['editable']==1 && $row['indice']!=1 )
							{
								fwrite($out,'									<div class="form-group">
										<label for="mod_'.$row['clave_campo'].'" class="control-label">'.$row['clave_campo'].'</label>
										<input type="text" class="form-control" id="mod_'.$row['clave_campo'].'" name="mod_'.$row['clave_campo'].'" placeholder="'.$row['descripcion_campo'].'"');
								if($row['requerido']==1)
								{
									fwrite($out,' required');
								}
								fwrite($out, ' >'.PHP_EOL);
								fwrite($out, '									</div>'.PHP_EOL);
							}
						}
					
					
						fwrite($out, '									<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary" id="'.$row3['clave_tabla'].'_guardar_datos">Guardar datos</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>');
	
		fclose($out);
}
?>