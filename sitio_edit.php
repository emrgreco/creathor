<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos del sitio</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
	
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del SITIOS &raquo; campo_editar datos</h2>
			<hr />
			
			<?php
			// escaping, campo_additionally removing everything that could be (html/javascript-) code
			$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
			$sql = mysqli_query($con, "SELECT * FROM sitio WHERE id_sitio='$nik'");
			if(mysqli_num_rows($sql) == 0){
				header("Location: SITIOS.php");
			}else{
				$row = mysqli_fetch_assoc($sql);
			}
			if(isset($_POST['save'])){
				$clave_sitio		     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$nombre_sitio		     = mysqli_real_escape_string($con,(strip_tags($_POST["nombre_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_sitio	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at	 =  date("Y-m-d H:i:s"); 
			

				$update = mysqli_query($con, "UPDATE sitio SET clave_sitio='$clave_sitio', nombre_sitio='$nombre_sitio', descripcion_sitio='$descripcion_sitio', update_at='$update_at' WHERE id_sitio='$nik'") or die(mysqli_error());
				if($update){
					header("Location: campo_edit.php?nik=".$nik."&pesan=sukses");
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
				}
			}
			
			if(isset($_GET['pesan']) == 'sukses'){
				echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
							header("Location: SITIOS.php");	
			}
			?>
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">clave</label>
					<div class="col-sm-2">
						<input type="text" name="clave_sitio" value="<?php echo $row ['clave_sitio']; ?>" class="form-control" placeholder="NIK" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">nombre_sitio</label>
					<div class="col-sm-4">
						<input type="text" name="nombre_sitio" value="<?php echo $row ['nombre_sitio']; ?>" class="form-control" placeholder="nombre_sitio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_sitio</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_sitio" value="<?php echo $row ['descripcion_sitio']; ?>" class="form-control" placeholder="descripcion_sitio" required>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="SITIOS.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>