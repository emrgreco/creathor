<?php
include("conexion.php");

if(!isset($_GET['SITE'])||!isset($_GET['TABLE']))
{
	header("Location: SITIOS.php");
}
else
{
	$site=$_GET['SITE'];
	$table=$_GET['TABLE'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos de campos</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
	
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del campos &raquo; campo_editar datos</h2>
			<hr />
			
			<?php
			// escaping, campo_additionally removing everything that could be (html/javascript-) code
			$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
			$sql = mysqli_query($con, "SELECT * FROM campos WHERE id_campo='$nik'");
			if(mysqli_num_rows($sql) == 0){
				header("Location: CAMPOS.php?SITE=$site&TABLE=$table");
			}else{
				$row = mysqli_fetch_assoc($sql);
			}
			if(isset($_POST['save'])){
				$clave_campo		     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_campo	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at	 =  date("Y-m-d H:i:s"); 
				$tipo_campo			 = mysqli_real_escape_string($con,(strip_tags($_POST["tipo_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$longitud_campo			 = mysqli_real_escape_string($con,(strip_tags($_POST["longitud_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$carpeta			 = mysqli_real_escape_string($con,(strip_tags($_POST["carpeta"],ENT_QUOTES)));//Escanpando caracteres 
				$visible			 = (mysqli_real_escape_string($con,(strip_tags($_POST["visible"],ENT_QUOTES)))=='on')?1:0;
				$editable			 = (mysqli_real_escape_string($con,(strip_tags($_POST["editable"],ENT_QUOTES)))=='on')?1:0;
				$requerido			 = (mysqli_real_escape_string($con,(strip_tags($_POST["requerido"],ENT_QUOTES)))=='on')?1:0;
				$indice				 = (mysqli_real_escape_string($con,(strip_tags($_POST["indice"],ENT_QUOTES)))=='on')?1:0;
				$busqueda			 = (mysqli_real_escape_string($con,(strip_tags($_POST["busqueda"],ENT_QUOTES)))=='on')?1:0;

				$jpeg                =(mysqli_real_escape_string($con,(strip_tags($_POST["jpeg"],ENT_QUOTES)))=='on')?1:0;
				$png                 =(mysqli_real_escape_string($con,(strip_tags($_POST["png"],ENT_QUOTES)))=='on')?1:0;
				$gif                 =(mysqli_real_escape_string($con,(strip_tags($_POST["gif"],ENT_QUOTES)))=='on')?1:0;
				$tif                 =(mysqli_real_escape_string($con,(strip_tags($_POST["tif"],ENT_QUOTES)))=='on')?1:0;
				$svg                 =(mysqli_real_escape_string($con,(strip_tags($_POST["svg"],ENT_QUOTES)))=='on')?1:0;
				$eps                 =(mysqli_real_escape_string($con,(strip_tags($_POST["eps"],ENT_QUOTES)))=='on')?1:0;
				$pdf                 =(mysqli_real_escape_string($con,(strip_tags($_POST["pdf"],ENT_QUOTES)))=='on')?1:0;
				$xlsx                =(mysqli_real_escape_string($con,(strip_tags($_POST["xlsx"],ENT_QUOTES)))=='on')?1:0;
				$xlsm                =(mysqli_real_escape_string($con,(strip_tags($_POST["xlsm"],ENT_QUOTES)))=='on')?1:0;
				$doc                 =(mysqli_real_escape_string($con,(strip_tags($_POST["doc"],ENT_QUOTES)))=='on')?1:0;
				$docx                =(mysqli_real_escape_string($con,(strip_tags($_POST["docx"],ENT_QUOTES)))=='on')?1:0;
				$dwg                 =(mysqli_real_escape_string($con,(strip_tags($_POST["dwg"],ENT_QUOTES)))=='on')?1:0;
				$txt                 =(mysqli_real_escape_string($con,(strip_tags($_POST["txt"],ENT_QUOTES)))=='on')?1:0;
				$zip                 =(mysqli_real_escape_string($con,(strip_tags($_POST["zip"],ENT_QUOTES)))=='on')?1:0;
				$rar                 =(mysqli_real_escape_string($con,(strip_tags($_POST["rar"],ENT_QUOTES)))=='on')?1:0;
			

				$update = mysqli_query($con, "UPDATE campos SET clave_campo='$clave_campo', descripcion_campo='$descripcion_campo', tipo_campo='$tipo_campo' ,longitud_campo='$longitud_campo', update_at='$update_at', editable='$editable', requerido='$requerido', indice='$indice', busqueda='$busqueda', visible='$visible', carpeta='$carpeta', jpeg='$jpeg', png='$png', gif='$gif', tif='$tif', svg='$svg', eps='$eps', pdf='$pdf', xlsx='$xlsx', xlsm='$xlsm', doc='$doc', docx='$docx', dwg='$dwg', txt='$txt', zip='$zip', rar='$rar' 	WHERE id_campo='$nik'") or die(mysqli_error());

				if($update){
					header("Location: CAMPOS.php?SITE=$site&TABLE=$table&nik=$nik&pesan=sukses");
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
				}
			}
			
			if(isset($_GET['pesan']) == 'sukses'){
				echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
							header("Location: CAMPOS.php");	
			}
			?>
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">clave</label>
					<div class="col-sm-2">
						<input type="text" name="clave_campo" value="<?php echo $row ['clave_campo']; ?>" class="form-control" placeholder="NIK" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_campo</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_campo" value="<?php echo $row ['descripcion_campo']; ?>" class="form-control" placeholder="descripcion_campo" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">tipo_campo</label>
					<div class="col-sm-3">
						<select name="tipo_campo" class="form-control">
							<option value="">- Selecciona tipo_campo -</option>
							<option value="1"<?php if ($row ['tipo_campo']==1){echo "selected";} ?>>INT</option>
							<option value="2"<?php if ($row ['tipo_campo']==2){echo "selected";} ?>>VARCHAR</option>
							<option value="3"<?php if ($row ['tipo_campo']==3){echo "selected";} ?>>DATE</option>
							<option value="4"<?php if ($row ['tipo_campo']==4){echo "selected";} ?>>DATETIME</option>
							<option value="5"<?php if ($row ['tipo_campo']==5){echo "selected";} ?>>TEXT</option>
							<option value="6"<?php if ($row ['tipo_campo']==6){echo "selected";} ?>>FILE</option>
							<option value="7"<?php if ($row ['tipo_campo']==7){echo "selected";} ?>>BOOLEAN</option>
							<option value="8"<?php if ($row ['tipo_campo']==8){echo "selected";} ?>>DOUBLE</option>
						</select> 
					</div>
                   
                </div>
			

				<div class="form-group">
					<label class="col-sm-3 control-label">longitud_campo</label>
					<div class="col-sm-4">
						<input type="number" name="longitud_campo" value="<?php echo $row ['longitud_campo']; ?>" class="form-control" placeholder="longitud_campo" required>
					</div>
				</div>



				<div class="form-group" >
					<label class="col-sm-3 control-label">carpeta</label>
					<div class="col-sm-4">
						<input type="text" name="carpeta" id="carpeta" value="<?php echo $row ['carpeta']; ?>" class="form-control" placeholder="carpeta" <?php if ($row ['tipo_campo']!=6){echo "disabled";} ?> >
					</div>
				</div>


				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">jpeg</label>
						<input type="checkbox" name="jpeg" class="form-control" placeholder="jpeg" <?php if($row ['jpeg']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">png</label>
						<input type="checkbox" name="png" class="form-control" placeholder="png" <?php if($row ['png']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">gif</label>
						<input type="checkbox" name="gif" class="form-control" placeholder="gif" <?php if($row ['gif']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">tif</label>
						<input type="checkbox" name="tif" class="form-control" placeholder="tif" <?php if($row ['tif']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">svg</label>
						<input type="checkbox" name="svg" class="form-control" placeholder="svg" <?php if($row ['svg']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">eps</label>
						<input type="checkbox" name="eps" class="form-control" placeholder="eps" <?php if($row ['eps']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
				</div>
				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">pdf</label>
						<input type="checkbox" name="pdf" class="form-control" placeholder="pdf" <?php if($row ['pdf']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">xlsx</label>
						<input type="checkbox" name="xlsx" class="form-control" placeholder="xlsx" <?php if($row ['xlsx']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">xlsm</label>
						<input type="checkbox" name="xlsm" class="form-control" placeholder="xlsm" <?php if($row ['xlsm']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">doc</label>
						<input type="checkbox" name="doc" class="form-control" placeholder="doc" <?php if($row ['doc']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">docx</label>
						<input type="checkbox" name="docx" class="form-control" placeholder="docx" <?php if($row ['docx']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">dwg</label>
						<input type="checkbox" name="dwg" class="form-control" placeholder="dwg" <?php if($row ['dwg']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
				</div>
				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">txt</label>
						<input type="checkbox" name="txt" class="form-control" placeholder="txt" <?php if($row ['txt']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">zip</label>
						<input type="checkbox" name="zip" class="form-control" placeholder="zip" <?php if($row ['zip']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">rar</label>
						<input type="checkbox" name="rar" class="form-control" placeholder="rar" <?php if($row ['rar']==1) echo "checked "; if ($row ['tipo_campo']!=6){echo "disabled";}?>>
					</div>
					</center>
				</div>



				<div class="form-group">
					<label class="col-sm-3 control-label">indice</label>
					<div class="col-sm-4">
						<input type="checkbox" name="indice" class="form-control" placeholder="indice" <?php if($row ['indice']==1) echo "checked"; ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">visible</label>
					<div class="col-sm-4">
						<input type="checkbox" name="visible" class="form-control" placeholder="visible" <?php if($row ['visible']==1) echo "checked"; ?>>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">editable</label>
					<div class="col-sm-4">
						<input type="checkbox" name="editable" class="form-control" placeholder="editable" <?php if($row ['editable']==1) echo "checked"; ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">requerido</label>
					<div class="col-sm-4">
						<input type="checkbox" name="requerido" class="form-control" placeholder="requerido" <?php if($row ['requerido']==1) echo "checked"; ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">busqueda</label>
					<div class="col-sm-4">
						<input type="checkbox" name="busqueda" class="form-control" placeholder="busqueda" <?php if($row ['busqueda']==1) echo "checked"; ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="CAMPOS.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>


	$('select').on('change', function() {
		if( this.value == 6 )
		{
			$( "#carpeta" ).prop("disabled", false);
			$( "#jpeg" ).prop("disabled", false);
			$( "#png" ).prop("disabled", false);
			$( "#gif" ).prop("disabled", false);
			$( "#tif" ).prop("disabled", false);
			$( "#svg" ).prop("disabled", false);
			$( "#eps" ).prop("disabled", false);
			$( "#pdf" ).prop("disabled", false);
			$( "#lsx" ).prop("disabled", false);
			$( "#lsm" ).prop("disabled", false);
			$( "#doc" ).prop("disabled", false);
			$( "#ocx" ).prop("disabled", false);
			$( "#dwg" ).prop("disabled", false);
			$( "#txt" ).prop("disabled", false);
			$( "#zip" ).prop("disabled", false);
			$( "#rar" ).prop("disabled", false);
		}
		else
		{
			$( "#carpeta" ).prop( "disabled", true );
			$( "#jpeg" ).prop("disabled", true);
			$( "#png" ).prop("disabled", true);
			$( "#gif" ).prop("disabled", true);
			$( "#tif" ).prop("disabled", true);
			$( "#svg" ).prop("disabled", true);
			$( "#eps" ).prop("disabled", true);
			$( "#pdf" ).prop("disabled", true);
			$( "#lsx" ).prop("disabled", true);
			$( "#lsm" ).prop("disabled", true);
			$( "#doc" ).prop("disabled", true);
			$( "#ocx" ).prop("disabled", true);
			$( "#dwg" ).prop("disabled", true);
			$( "#txt" ).prop("disabled", true);
			$( "#zip" ).prop("disabled", true);
			$( "#rar" ).prop("disabled", true);
		}
	});


	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>