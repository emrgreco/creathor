<?php
include("conexion.php");

$SITIO="";
$TABLA="";
$CAMPO="active";

if(!isset($_GET['SITE'])||!isset($_GET['TABLE']))
{
	header("Location: SITIOS.php");
}
else
{
	$site=$_GET['SITE'];
	$table=$_GET['TABLE'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos de campos</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">

	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include('nav.php');?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Lista de campos</h2>
			<hr />

			<?php
			if(isset($_GET['aksi']) == 'delete'){
				// escaping, campo_additionally removing everything that could be (html/javascript-) code
				$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
				$cek = mysqli_query($con, "SELECT * FROM campos WHERE id_campo='$nik'");
				if(mysqli_num_rows($cek) == 0){
					echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
				}else{
					$delete = mysqli_query($con, "DELETE FROM campos WHERE id_campo='$nik'");
					if($delete){
						echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
						
							header("Location: CAMPOS.php?SITE=$site&TABLE=$table");
					}else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.</div>';
					}
				}
			}
			?>

			<form class="form-inline" method="get">
				<div class="form-group">
					<select name="filter" class="form-control" onchange="form.submit()">
						<?php $filter = (isset($_GET['filter']) ? strtolower($_GET['filter']) : NULL);  ?>
						<option value="0"<?php if($filter == 'Filtros de campos'){ echo 'selected'; } ?>>Filtros de campos</option>
						<option value="1" <?php if($filter == 'INT'){ echo 'selected'; } ?>>INT</option>
						<option value="2" <?php if($filter == 'VARCHAR'){ echo 'selected'; } ?>>VARCHAR</option>
                        <option value="3" <?php if($filter == 'DATE'){ echo 'selected'; } ?>>DATE</option>
                        <option value="4" <?php if($filter == 'DATETIME'){ echo 'selected'; } ?>>DATETIME</option>
                        <option value="5" <?php if($filter == 'TEXT'){ echo 'selected'; } ?>>TEXT</option>
                        <option value="6" <?php if($filter == 'FILE'){ echo 'selected'; } ?>>FILE</option>
                        <option value="7" <?php if($filter == 'BOOLEAN'){ echo 'selected'; } ?>>BOOLEAN</option>
                        <option value="8" <?php if($filter == 'DOUBLE'){ echo 'selected'; } ?>>DOUBLE</option>
					</select>
				</div>
			</form>
			<a href="campo_add.php?SITE=<?php echo $site; ?>&TABLE=<?php echo $table; ?>">Agregar campo</a>
			<br />
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
                    <th>num</th>
					<th>Clave</th>
                    <th>descripción</th>
					<th>tipo</th>
					<th>longitud</th>
                    <th>Acciones</th>
				</tr>
				<?php
				if($filter!=0){
					$sql = mysqli_query($con, "SELECT * FROM campos WHERE id_sitio=$site and id_tabla=$table and tipo_campo='$filter' ORDER BY id_campo ASC");
				}else{
					$sql = mysqli_query($con, "SELECT * FROM campos WHERE id_sitio=$site and id_tabla=$table ORDER BY id_campo ASC");
				}
				if(mysqli_num_rows($sql) == 0){
					echo '<tr><td colspan="8">No hay datos.</td></tr>';
				}else{
					$no = 1;
					while($row = mysqli_fetch_assoc($sql)){
						echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$row['clave_campo'].'</td>
							<td>'.$row['descripcion_campo'].'</td><td>';
							if($row['tipo_campo'] == '1'){
								echo '<span class="label label-success">INT</span>';
							}
                            else if ($row['tipo_campo'] == '2' ){
								echo '<span class="label label-info">VARCHAR</span>';
							}
                            else if ($row['tipo_campo'] == '3' ){
								echo '<span class="label label-warning">DATE</span>';
							}
                            else if ($row['tipo_campo'] == '4' ){
								echo '<span class="label label-primary">DATETIME</span>';
							}
                            else if ($row['tipo_campo'] == '5' ){
								echo '<span class="label label-warning">TEXT</span>';
							}
                            else if ($row['tipo_campo'] == '6' ){
								echo '<span class="label label-primary">FILE</span>';
							}
                            else if ($row['tipo_campo'] == '7' ){
								echo '<span class="label label-primary">BOOLEAN</span>';
							}
                            else if ($row['tipo_campo'] == '8' ){
								echo '<span class="label label-warning">DOUBLE</span>';
							}
						echo '
							</td>
							<td>'.$row['longitud_campo'].'</td>
							<td>

								<a href="campo_edit.php?SITE='.$site.'&TABLE='.$table.'&nik='.$row['id_campo'].'" title="campo_editar datos" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
								<a href="CAMPOS.php?SITE='.$site.'&TABLE='.$table.'&aksi=delete&nik='.$row['id_campo'].'" title="Eliminar" onclick="return confirm(\'Esta seguro de borrar los datos '.$row['clave_campo'].'?\')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
						';
						$no++;
					}
				}
				?>
			</table>
			</div>
		</div>
	</div><center>
	<p>&copy; diseño <?php echo date("Y");?></p
		</center>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
