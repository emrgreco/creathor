<?php
include("conexion.php");


$SITIO="";
$TABLA="active";
$CAMPO="";

if(!isset($_GET['SITE']))
{
	//header("Location: SITIOS.php");
}
else
{
	$site=$_GET['SITE'];
}

?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos de tablaS</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">

	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include('nav.php');?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Lista de tablas</h2>
			<hr />

			<?php
			if(isset($_GET['aksi']) == 'delete')
			{
				// escaping, campo_additionally removing everything that could be (html/javascript-) code
				$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
				$cek = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='$nik'");


				$LOASA = mysqli_query($con, "SELECT * FROM tabla WHERE tabla_principal='$nik'");
				$ASMHASNA = mysqli_fetch_assoc($LOASA);

				if(mysqli_num_rows($cek) == 0)
				{
					echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
				}
				else if (mysqli_num_rows($LOASA) == 0)
				{
					$delete = mysqli_query($con, "DELETE FROM tabla WHERE id_tabla='$nik'");
					$delete = mysqli_query($con, "DELETE FROM campos WHERE id_tabla='$nik'");
					if($delete)
					{
						echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
					}
					else
					{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.</div>';
					}
				}
				else
				{
						echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Existen tablas secundarias asociadas a esta tabla</div>';
				}
			}
			?>
			<a href="tabla_add.php?SITE=<?php echo $site; ?>">Agregar tabla</a>
			<br>
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
                    <th>Icono</th>
					<th>Clave</th>
                    <th>Descripción</th>
                    <th>Tipo Modificación</th>
                    <th>Tipo Tabla</th>
                    <th>Acciones</th>
				</tr>
				<?php

				$sql = mysqli_query($con, "SELECT * FROM tabla where id_sitio=$site");
				if(mysqli_num_rows($sql) == 0)
				{
					echo '<tr><td colspan="8">No hay datos.</td></tr>';
				}else{
					$no = 1;
					while($row = mysqli_fetch_assoc($sql))
					{
						echo '
						<tr>
							<td> ';
							echo '<span class="'.icono_glyphicon($row['icono']).'"></span>';
							echo ' </td>
							<td><a href="CAMPOS.php?SITE='.$site.'&TABLE='.$row['id_tabla'].'">'.$row['clave_tabla'].'</td>
							<td>'.$row['descripcion_tabla'].'</td>';
							echo '<td>';

							if($row['desplegable'] == '1')
							{
								echo '<span class="label label-success">MODAL</span>';
							}
                            else if ($row['desplegable'] == '2' )
                            {
								echo '<span class="label label-info">PÁGINA</span>';

							}
						echo '</td>';
						
						echo '<td>';

							if($row['tipo_tabla'] == '1')
							{
								echo '<span class="label label-success">PRINCIPAL</span>';
							}
                            else if($row['tipo_tabla'] == '2')
                            {
								$sql18 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$row['tabla_principal']."';");
								$row23=mysqli_fetch_assoc($sql18);
								echo '<span class="label label-warning">SECUNDARIA / '.$row23['clave_tabla'].'</span>';
							}
                            else if($row['tipo_tabla'] == '3')
                            {
								$sql18 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$row['tabla_principal']."';");
								$row23=mysqli_fetch_assoc($sql18);

								$sql33 = mysqli_query($con, "SELECT * FROM tabla WHERE id_tabla='".$row['tabla_liga']."';");
								$row35=mysqli_fetch_assoc($sql33);

								echo '<span class="label label-danger">'.$row23['clave_tabla'].' / '.$row35['clave_tabla'].'</span>';
							}
						echo '</td>';

						echo '<td>
								<a href="tabla_edit.php?SITE='.$site.'&nik='.$row['id_tabla'].'" title="campo_editar datos" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

								<a href="TABLAS.php?SITE='.$site.'&aksi=delete&nik='.$row['id_tabla'].'" title="Eliminar" onclick="return confirm(\'Esta seguro de borrar los datos '.$row['clave_tabla'].'?\')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
						';
						$no++;
					}
				}
				?>
			</table>
			</div>
		</div>
	</div><center>
	<p>&copy; diseño <?php echo date("Y");?></p></center>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>

<?php 
		
	
 ?>