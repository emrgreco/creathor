<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Creathor</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del campos &raquo; Agregar datos</h2>
			<hr />

			<?php
			if(isset($_POST['campo_add'])){
				$clave_sitio		     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$nombre_sitio		     = mysqli_real_escape_string($con,(strip_tags($_POST["nombre_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_sitio	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_sitio"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at	 =  date("Y-m-d H:i:s"); 
				$create_at	 =  date("Y-m-d H:i:s"); 
			

				$cek = mysqli_query($con, "SELECT * FROM sitio WHERE clave_sitio='$clave_sitio'");
				if(mysqli_num_rows($cek) == 0)
				{
					echo"INSERT INTO sitio(clave_sitio, nombre_sitio, descripcion_sitio, create_at, update_at)
															VALUES('$clave_sitio','$nombre_sitio', '$descripcion_sitio','$create_at', '$update_at')";
						$insert = mysqli_query($con, "INSERT INTO sitio(clave_sitio, nombre_sitio, descripcion_sitio, create_at, update_at)
															VALUES('$clave_sitio','$nombre_sitio', '$descripcion_sitio','$create_at', '$update_at')") or die(mysqli_error());
						if($insert){
							echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
							
							header("Location: SITIOS.php");
						}else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
						}
					 
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. Clave_sitio exite!</div>';
				}
			}
			?>

			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">Clave_sitio</label>
					<div class="col-sm-2">
						<input type="text" name="clave_sitio" class="form-control" placeholder="Clave_sitio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">nombre_sitio</label>
					<div class="col-sm-4">
						<input type="text" name="nombre_sitio" class="form-control" placeholder="nombre_sitio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_sitio</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_sitio" class="form-control" placeholder="descripcion_sitio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="campo_add" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="CAMPOS.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>
