<?php
/*Datos de conexion a la base de datos*/
$db_host = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "creathor";

$con = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if(mysqli_connect_errno()){
	echo 'No se pudo conectar a la base de datos : '.mysqli_connect_error();
}





function formatos_permitidos_sin_comillas($jpeg, $png ,$gif ,$tif ,$svg ,$eps ,$pdf ,$xlsx, $xlsm, $doc ,$docx ,$dwg ,$txt ,$zip ,$rar)
{
	$cadena='';
	if($jpeg==1)
		$cadena.= '.jpeg, ';
	if($png ==1)
		$cadena.= '.png, ';
	if($gif ==1)
		$cadena.= '.gif, ';
	if($tif ==1)
		$cadena.= '.tif, ';
	if($svg ==1)
		$cadena.= '.svg, ';
	if($eps ==1)
		$cadena.= '.eps, ';
	if($pdf ==1)
		$cadena.= '.pdf, ';
	if($xlsx==1)
		$cadena.= '.xlsx, ';
	if($xlsm==1)
		$cadena.= '.xlsm, ';
	if($doc ==1)
		$cadena.= '.doc, ';
	if($docx==1)
		$cadena.= '.docx, ';
	if($dwg ==1)
		$cadena.= '.dwg, ';
	if($txt ==1)
		$cadena.= '.txt, ';
	if($zip ==1)
		$cadena.= '.zip, ';
	if($rar ==1)
		$cadena.= '.rar, ';
	$cadena=substr($cadena, 0, -2);
	return $cadena;
}


function formatos_permitidos($jpeg, $png ,$gif ,$tif ,$svg ,$eps ,$pdf ,$xlsx, $xlsm, $doc ,$docx ,$dwg ,$txt ,$zip ,$rar)
{
	$cadena='';
	if($jpeg==1)
		$cadena.= '"jpeg", ';
	if($png ==1)
		$cadena.= '"png", ';
	if($gif ==1)
		$cadena.= '"gif", ';
	if($tif ==1)
		$cadena.= '"tif", ';
	if($svg ==1)
		$cadena.= '"svg", ';
	if($eps ==1)
		$cadena.= '"eps", ';
	if($pdf ==1)
		$cadena.= '"pdf", ';
	if($xlsx==1)
		$cadena.= '"xlsx", ';
	if($xlsm==1)
		$cadena.= '"xlsm", ';
	if($doc ==1)
		$cadena.= '"doc", ';
	if($docx==1)
		$cadena.= '"docx", ';
	if($dwg ==1)
		$cadena.= '"dwg", ';
	if($txt ==1)
		$cadena.= '"txt", ';
	if($zip ==1)
		$cadena.= '"zip", ';
	if($rar ==1)
		$cadena.= '"rar", ';
	$cadena=substr($cadena, 0, -2);
	return $cadena;
}


function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755))
    {
        $result=false;
       $dirsource="";
        if (is_file($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if (!file_exists($dest)) {
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true);
                }
                $__dest=$dest."/".basename($source);
            } else {
                $__dest=$dest;
            }
            $result=copy($source, $__dest);
            chmod($__dest,$options['filePermission']);
           
        } elseif(is_dir($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if ($source[strlen($source)-1]=='/') {
                    //Copy only contents
                } else {
                    //Change parent itself and its contents
                    $dest=$dest.basename($source);
                    @mkdir($dest);
                    chmod($dest,$options['filePermission']);
                }
            } else {
                if ($source[strlen($source)-1]=='/') {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                } else {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                }
            }

            $dirHandle=opendir($source);
            while($file=readdir($dirHandle))
            {
                if($file!="." && $file!="..")
                {
                    if(!is_dir($dirsource."/".$file)) {
                        $__dest=$dest."/".$file;
                    } else {
                        $__dest=$dest."/".$file;
                    }
                    //echo "$source/$file ||| $__dest<br />";
                    $result=smartCopy($source."/".$file, $__dest, $options);
                }
            }
            closedir($dirHandle);
           
        } else {
            $result=false;
        }
        return $result;
    }

    
	function icono($row3)
	{
		switch ($row3)
		{
			case '1': 
				return "fas fa-xs fa-asterisk";
				break;
			case '2': 
				return "fas fa-xs fa-plus";
				break;
			case '3': 
				return "fas fa-xs fa-euro";
				break;
			case '4': 
				return "fas fa-xs fa-eur";
				break;
			case '5': 
				return "fas fa-xs fa-minus";
				break;
			case '6': 
				return "fas fa-xs fa-cloud";
				break;
			case '7': 
				return "fas fa-xs fa-envelope";
				break;
			case '8': 
				return "fas fa-xs fa-pencil";
				break;
			case '9': 
				return "fas fa-xs fa-glass";
				break;
			case '10': 
				return "fas fa-xs fa-music";
				break;
			case '11': 
				return "fas fa-xs fa-search";
				break;
			case '12': 
				return "fas fa-xs fa-heart";
				break;
			case '13': 
				return "fas fa-xs fa-star";
				break;
			case '14': 
				return "fas fa-xs fa-star-empty";
				break;
			case '15': 
				return "fas fa-xs fa-user";
				break;
			case '16': 
				return "fas fa-xs fa-film";
				break;
			case '17': 
				return "fas fa-xs fa-th-large";
				break;
			case '18': 
				return "fas fa-xs fa-th";
				break;
			case '19': 
				return "fas fa-xs fa-th-list";
				break;
			case '20': 
				return "fas fa-xs fa-ok";
				break;
			case '21': 
				return "fas fa-xs fa-remove";
				break;
			case '22': 
				return "fas fa-xs fa-zoom-in";
				break;
			case '23': 
				return "fas fa-xs fa-zoom-out";
				break;
			case '24': 
				return "fas fa-xs fa-off";
				break;
			case '25': 
				return "fas fa-xs fa-signal";
				break;
			case '26': 
				return "fas fa-xs fa-cog";
				break;
			case '27': 
				return "fas fa-xs fa-trash";
				break;
			case '28': 
				return "fas fa-xs fa-home";
				break;
			case '29': 
				return "fas fa-xs fa-file";
				break;
			case '30': 
				return "fas fa-xs fa-time";
				break;
			case '31': 
				return "fas fa-xs fa-road";
				break;
			case '32': 
				return "fas fa-xs fa-download-alt";
				break;
			case '33': 
				return "fas fa-xs fa-download";
				break;
			case '34': 
				return "fas fa-xs fa-upload";
				break;
			case '35': 
				return "fas fa-xs fa-inbox";
				break;
			case '36': 
				return "fas fa-xs fa-play-circle";
				break;
			case '37': 
				return "fas fa-xs fa-repeat";
				break;
			case '38': 
				return "fas fa-xs fa-refresh";
				break;
			case '39': 
				return "fas fa-xs fa-list-alt";
				break;
			case '40': 
				return "fas fa-xs fa-lock";
				break;
			case '41': 
				return "fas fa-xs fa-flag";
				break;
			case '42': 
				return "fas fa-xs fa-headphones";
				break;
			case '43': 
				return "fas fa-xs fa-volume-off";
				break;
			case '44': 
				return "fas fa-xs fa-volume-down";
				break;
			case '45': 
				return "fas fa-xs fa-volume-up";
				break;
			case '46': 
				return "fas fa-xs fa-qrcode";
				break;
			case '47': 
				return "fas fa-xs fa-barcode";
				break;
			case '48': 
				return "fas fa-xs fa-tag";
				break;
			case '49': 
				return "fas fa-xs fa-tags";
				break;
			case '50': 
				return "fas fa-xs fa-book";
				break;
			case '51': 
				return "fas fa-xs fa-bookmark";
				break;
			case '52': 
				return "fas fa-xs fa-print";
				break;
			case '53': 
				return "fas fa-xs fa-camera";
				break;
			case '54': 
				return "fas fa-xs fa-font";
				break;
			case '55': 
				return "fas fa-xs fa-bold";
				break;
			case '56': 
				return "fas fa-xs fa-italic";
				break;
			case '57': 
				return "fas fa-xs fa-text-height";
				break;
			case '58': 
				return "fas fa-xs fa-text-width";
				break;
			case '59': 
				return "fas fa-xs fa-align-left";
				break;
			case '60': 
				return "fas fa-xs fa-align-center";
				break;
			case '61': 
				return "fas fa-xs fa-align-right";
				break;
			case '62': 
				return "fas fa-xs fa-align-justify";
				break;
			case '63': 
				return "fas fa-xs fa-list";
				break;
			case '64': 
				return "fas fa-xs fa-indent-left";
				break;
			case '65': 
				return "fas fa-xs fa-indent-right";
				break;
			case '66': 
				return "fas fa-xs fa-facetime-video";
				break;
			case '67': 
				return "fas fa-xs fa-picture";
				break;
			case '68': 
				return "fas fa-xs fa-map-marker";
				break;
			case '69': 
				return "fas fa-xs fa-adjust";
				break;
			case '70': 
				return "fas fa-xs fa-tint";
				break;
			case '71': 
				return "fas fa-xs fa-edit";
				break;
			case '72': 
				return "fas fa-xs fa-share";
				break;
			case '73': 
				return "fas fa-xs fa-check";
				break;
			case '74': 
				return "fas fa-xs fa-move";
				break;
			case '75': 
				return "fas fa-xs fa-step-backward";
				break;
			case '76': 
				return "fas fa-xs fa-fast-backward";
				break;
			case '77': 
				return "fas fa-xs fa-backward";
				break;
			case '78': 
				return "fas fa-xs fa-play";
				break;
			case '79': 
				return "fas fa-xs fa-pause";
				break;
			case '80': 
				return "fas fa-xs fa-stop";
				break;
			case '81': 
				return "fas fa-xs fa-forward";
				break;
			case '82': 
				return "fas fa-xs fa-fast-forward";
				break;
			case '83': 
				return "fas fa-xs fa-step-forward";
				break;
			case '84': 
				return "fas fa-xs fa-eject";
				break;
			case '85': 
				return "fas fa-xs fa-chevron-left";
				break;
			case '86': 
				return "fas fa-xs fa-chevron-right";
				break;
			case '87': 
				return "fas fa-xs fa-plus-sign";
				break;
			case '88': 
				return "fas fa-xs fa-minus-sign";
				break;
			case '89': 
				return "fas fa-xs fa-remove-sign";
				break;
			case '90': 
				return "fas fa-xs fa-ok-sign";
				break;
			case '91': 
				return "fas fa-xs fa-question-sign";
				break;
			case '92': 
				return "fas fa-xs fa-info-sign";
				break;
			case '93': 
				return "fas fa-xs fa-screenshot";
				break;
			case '94': 
				return "fas fa-xs fa-remove-circle";
				break;
			case '95': 
				return "fas fa-xs fa-ok-circle";
				break;
			case '96': 
				return "fas fa-xs fa-ban-circle";
				break;
			case '97': 
				return "fas fa-xs fa-arrow-left";
				break;
			case '98': 
				return "fas fa-xs fa-arrow-right";
				break;
			case '99': 
				return "fas fa-xs fa-arrow-up";
				break;
			case '100': 
				return "fas fa-xs fa-arrow-down";
				break;
			case '101': 
				return "fas fa-xs fa-share-alt";
				break;
			case '102': 
				return "fas fa-xs fa-resize-full";
				break;
			case '103': 
				return "fas fa-xs fa-resize-small";
				break;
			case '104': 
				return "fas fa-xs fa-exclamation-sign";
				break;
			case '105': 
				return "fas fa-xs fa-gift";
				break;
			case '106': 
				return "fas fa-xs fa-leaf";
				break;
			case '107': 
				return "fas fa-xs fa-fire";
				break;
			case '108': 
				return "fas fa-xs fa-eye-open";
				break;
			case '109': 
				return "fas fa-xs fa-eye-close";
				break;
			case '110': 
				return "fas fa-xs fa-warning-sign";
				break;
			case '111': 
				return "fas fa-xs fa-plane";
				break;
			case '112': 
				return "fas fa-xs fa-calendar";
				break;
			case '113': 
				return "fas fa-xs fa-random";
				break;
			case '114': 
				return "fas fa-xs fa-comment";
				break;
			case '115': 
				return "fas fa-xs fa-magnet";
				break;
			case '116': 
				return "fas fa-xs fa-chevron-up";
				break;
			case '117': 
				return "fas fa-xs fa-chevron-down";
				break;
			case '118': 
				return "fas fa-xs fa-retweet";
				break;
			case '119': 
				return "fas fa-xs fa-shopping-cart";
				break;
			case '120': 
				return "fas fa-xs fa-folder-close";
				break;
			case '121': 
				return "fas fa-xs fa-folder-open";
				break;
			case '122': 
				return "fas fa-xs fa-resize-vertical";
				break;
			case '123': 
				return "fas fa-xs fa-resize-horizontal";
				break;
			case '124': 
				return "fas fa-xs fa-hdd";
				break;
			case '125': 
				return "fas fa-xs fa-bullhorn";
				break;
			case '126': 
				return "fas fa-xs fa-bell";
				break;
			case '127': 
				return "fas fa-xs fa-certificate";
				break;
			case '128': 
				return "fas fa-xs fa-thumbs-up";
				break;
			case '129': 
				return "fas fa-xs fa-thumbs-down";
				break;
			case '130': 
				return "fas fa-xs fa-hand-right";
				break;
			case '131': 
				return "fas fa-xs fa-hand-left";
				break;
			case '132': 
				return "fas fa-xs fa-hand-up";
				break;
			case '133': 
				return "fas fa-xs fa-hand-down";
				break;
			case '134': 
				return "fas fa-xs fa-circle-arrow-right";
				break;
			case '135': 
				return "fas fa-xs fa-circle-arrow-left";
				break;
			case '136': 
				return "fas fa-xs fa-circle-arrow-up";
				break;
			case '137': 
				return "fas fa-xs fa-circle-arrow-down";
				break;
			case '138': 
				return "fas fa-xs fa-globe";
				break;
			case '139': 
				return "fas fa-xs fa-wrench";
				break;
			case '140': 
				return "fas fa-xs fa-tasks";
				break;
			case '141': 
				return "fas fa-xs fa-filter";
				break;
			case '142': 
				return "fas fa-xs fa-briefcase";
				break;
			case '143': 
				return "fas fa-xs fa-fullscreen";
				break;
			case '144': 
				return "fas fa-xs fa-dashboard";
				break;
			case '145': 
				return "fas fa-xs fa-paperclip";
				break;
			case '146': 
				return "fas fa-xs fa-heart-empty";
				break;
			case '147': 
				return "fas fa-xs fa-link";
				break;
			case '148': 
				return "fas fa-xs fa-phone";
				break;
			case '149': 
				return "fas fa-xs fa-pushpin";
				break;
			case '150': 
				return "fas fa-xs fa-usd";
				break;
			case '151': 
				return "fas fa-xs fa-gbp";
				break;
			case '152': 
				return "fas fa-xs fa-sort";
				break;
			case '153': 
				return "fas fa-xs fa-sort-by-alphabet";
				break;
			case '154': 
				return "fas fa-xs fa-sort-by-alphabet-alt";
				break;
			case '155': 
				return "fas fa-xs fa-sort-by-order";
				break;
			case '156': 
				return "fas fa-xs fa-sort-by-order-alt";
				break;
			case '157': 
				return "fas fa-xs fa-sort-by-attributes";
				break;
			case '158': 
				return "fas fa-xs fa-sort-by-attributes-alt";
				break;
			case '159': 
				return "fas fa-xs fa-unchecked";
				break;
			case '160': 
				return "fas fa-xs fa-expand";
				break;
			case '161': 
				return "fas fa-xs fa-collapse-down";
				break;
			case '162': 
				return "fas fa-xs fa-collapse-up";
				break;
			case '163': 
				return "fas fa-xs fa-log-in";
				break;
			case '164': 
				return "fas fa-xs fa-flash";
				break;
			case '165': 
				return "fas fa-xs fa-log-out";
				break;
			case '166': 
				return "fas fa-xs fa-new-window";
				break;
			case '167': 
				return "fas fa-xs fa-record";
				break;
			case '168': 
				return "fas fa-xs fa-save";
				break;
			case '169': 
				return "fas fa-xs fa-open";
				break;
			case '170': 
				return "fas fa-xs fa-saved";
				break;
			case '171': 
				return "fas fa-xs fa-import";
				break;
			case '172': 
				return "fas fa-xs fa-export";
				break;
			case '173': 
				return "fas fa-xs fa-send";
				break;
			case '174': 
				return "fas fa-xs fa-floppy-disk";
				break;
			case '175': 
				return "fas fa-xs fa-floppy-saved";
				break;
			case '176': 
				return "fas fa-xs fa-floppy-remove";
				break;
			case '177': 
				return "fas fa-xs fa-floppy-save";
				break;
			case '178': 
				return "fas fa-xs fa-floppy-open";
				break;
			case '179': 
				return "fas fa-xs fa-credit-card";
				break;
			case '180': 
				return "fas fa-xs fa-transfer";
				break;
			case '181': 
				return "fas fa-xs fa-cutlery";
				break;
			case '182': 
				return "fas fa-xs fa-header";
				break;
			case '183': 
				return "fas fa-xs fa-compressed";
				break;
			case '184': 
				return "fas fa-xs fa-earphone";
				break;
			case '185': 
				return "fas fa-xs fa-phone-alt";
				break;
			case '186': 
				return "fas fa-xs fa-tower";
				break;
			case '187': 
				return "fas fa-xs fa-stats";
				break;
			case '188': 
				return "fas fa-xs fa-sd-video";
				break;
			case '189': 
				return "fas fa-xs fa-hd-video";
				break;
			case '190': 
				return "fas fa-xs fa-subtitles";
				break;
			case '191': 
				return "fas fa-xs fa-sound-stereo";
				break;
			case '192': 
				return "fas fa-xs fa-sound-dolby";
				break;
			case '193': 
				return "fas fa-xs fa-sound-5-1";
				break;
			case '194': 
				return "fas fa-xs fa-sound-6-1";
				break;
			case '195': 
				return "fas fa-xs fa-sound-7-1";
				break;
			case '196': 
				return "fas fa-xs fa-copyright-mark";
				break;
			case '197': 
				return "fas fa-xs fa-registration-mark";
				break;
			case '198': 
				return "fas fa-xs fa-cloud-download";
				break;
			case '199': 
				return "fas fa-xs fa-cloud-upload";
				break;
			case '200': 
				return "fas fa-xs fa-tree-conifer";
				break;
			case '201': 
				return "fas fa-xs fa-tree-deciduous";
				break;
			case '202': 
				return "fas fa-xs fa-cd";
				break;
			case '203': 
				return "fas fa-xs fa-save-file";
				break;
			case '204': 
				return "fas fa-xs fa-open-file";
				break;
			case '205': 
				return "fas fa-xs fa-level-up";
				break;
			case '206': 
				return "fas fa-xs fa-copy";
				break;
			case '207': 
				return "fas fa-xs fa-paste";
				break;
			case '208': 
				return "fas fa-xs fa-alert";
				break;
			case '209': 
				return "fas fa-xs fa-equalizer";
				break;
			case '210': 
				return "fas fa-xs fa-king";
				break;
			case '211': 
				return "fas fa-xs fa-queen";
				break;
			case '212': 
				return "fas fa-xs fa-pawn";
				break;
			case '213': 
				return "fas fa-xs fa-bishop";
				break;
			case '214': 
				return "fas fa-xs fa-knight";
				break;
			case '215': 
				return "fas fa-xs fa-baby-formula";
				break;
			case '216': 
				return "fas fa-xs fa-tent";
				break;
			case '217': 
				return "fas fa-xs fa-blackboard";
				break;
			case '218': 
				return "fas fa-xs fa-bed";
				break;
			case '219': 
				return "fas fa-xs fa-apple";
				break;
			case '220': 
				return "fas fa-xs fa-erase";
				break;
			case '221': 
				return "fas fa-xs fa-hourglass";
				break;
			case '222': 
				return "fas fa-xs fa-lamp";
				break;
			case '223': 
				return "fas fa-xs fa-duplicate";
				break;
			case '224': 
				return "fas fa-xs fa-piggy-bank";
				break;
			case '225': 
				return "fas fa-xs fa-scissors";
				break;
			case '226': 
				return "fas fa-xs fa-bitcoin";
				break;
			case '227': 
				return "fas fa-xs fa-btc";
				break;
			case '228': 
				return "fas fa-xs fa-xbt";
				break;
			case '229': 
				return "fas fa-xs fa-yen";
				break;
			case '230': 
				return "fas fa-xs fa-jpy";
				break;
			case '231': 
				return "fas fa-xs fa-ruble";
				break;
			case '232': 
				return "fas fa-xs fa-rub";
				break;
			case '233': 
				return "fas fa-xs fa-scale";
				break;
			case '234': 
				return "fas fa-xs fa-ice-lolly";
				break;
			case '235': 
				return "fas fa-xs fa-ice-lolly-tasted";
				break;
			case '236': 
				return "fas fa-xs fa-education";
				break;
			case '237': 
				return "fas fa-xs fa-option-horizontal";
				break;
			case '238': 
				return "fas fa-xs fa-option-vertical";
				break;
			case '239': 
				return "fas fa-xs fa-menu-hamburger";
				break;
			case '240': 
				return "fas fa-xs fa-modal-window";
				break;
			case '241': 
				return "fas fa-xs fa-oil";
				break;
			case '242': 
				return "fas fa-xs fa-grain";
				break;
			case '243': 
				return "fas fa-xs fa-sunglasses";
				break;
			case '244': 
				return "fas fa-xs fa-text-size";
				break;
			case '245': 
				return "fas fa-xs fa-text-color";
				break;
			case '246': 
				return "fas fa-xs fa-text-background";
				break;
			case '247': 
				return "fas fa-xs fa-object-align-top";
				break;
			case '248': 
				return "fas fa-xs fa-object-align-bottom";
				break;
			case '249': 
				return "fas fa-xs fa-object-align-horizontal";
				break;
			case '250': 
				return "fas fa-xs fa-object-align-left";
				break;
			case '251': 
				return "fas fa-xs fa-object-align-vertical";
				break;
			case '252': 
				return "fas fa-xs fa-object-align-right";
				break;
			case '253': 
				return "fas fa-xs fa-triangle-right";
				break;
			case '254': 
				return "fas fa-xs fa-triangle-left";
				break;
			case '255': 
				return "fas fa-xs fa-triangle-bottom";
				break;
			case '256': 
				return "fas fa-xs fa-triangle-top";
				break;
			case '257': 
				return "fas fa-xs fa-console";
				break;
			case '258': 
				return "fas fa-xs fa-superscript";
				break;
			case '259': 
				return "fas fa-xs fa-subscript";
				break;
			case '260': 
				return "fas fa-xs fa-menu-left";
				break;
			case '261': 
				return "fas fa-xs fa-menu-right";
				break;
			case '262': 
				return "fas fa-xs fa-menu-down";
				break;
			case '263': 
				return "fas fa-xs fa-menu-up";
				break;
			default:
				break;
		}
	}
	function icono_glyphicon($row3)
	{
		switch ($row3)
		{
			case '1': 
				return "glyphicon glyphicon-asterisk";
				break;
			case '2': 
				return "glyphicon glyphicon-plus";
				break;
			case '3': 
				return "glyphicon glyphicon-euro";
				break;
			case '4': 
				return "glyphicon glyphicon-eur";
				break;
			case '5': 
				return "glyphicon glyphicon-minus";
				break;
			case '6': 
				return "glyphicon glyphicon-cloud";
				break;
			case '7': 
				return "glyphicon glyphicon-envelope";
				break;
			case '8': 
				return "glyphicon glyphicon-pencil";
				break;
			case '9': 
				return "glyphicon glyphicon-glass";
				break;
			case '10': 
				return "glyphicon glyphicon-music";
				break;
			case '11': 
				return "glyphicon glyphicon-search";
				break;
			case '12': 
				return "glyphicon glyphicon-heart";
				break;
			case '13': 
				return "glyphicon glyphicon-star";
				break;
			case '14': 
				return "glyphicon glyphicon-star-empty";
				break;
			case '15': 
				return "glyphicon glyphicon-user";
				break;
			case '16': 
				return "glyphicon glyphicon-film";
				break;
			case '17': 
				return "glyphicon glyphicon-th-large";
				break;
			case '18': 
				return "glyphicon glyphicon-th";
				break;
			case '19': 
				return "glyphicon glyphicon-th-list";
				break;
			case '20': 
				return "glyphicon glyphicon-ok";
				break;
			case '21': 
				return "glyphicon glyphicon-remove";
				break;
			case '22': 
				return "glyphicon glyphicon-zoom-in";
				break;
			case '23': 
				return "glyphicon glyphicon-zoom-out";
				break;
			case '24': 
				return "glyphicon glyphicon-off";
				break;
			case '25': 
				return "glyphicon glyphicon-signal";
				break;
			case '26': 
				return "glyphicon glyphicon-cog";
				break;
			case '27': 
				return "glyphicon glyphicon-trash";
				break;
			case '28': 
				return "glyphicon glyphicon-home";
				break;
			case '29': 
				return "glyphicon glyphicon-file";
				break;
			case '30': 
				return "glyphicon glyphicon-time";
				break;
			case '31': 
				return "glyphicon glyphicon-road";
				break;
			case '32': 
				return "glyphicon glyphicon-download-alt";
				break;
			case '33': 
				return "glyphicon glyphicon-download";
				break;
			case '34': 
				return "glyphicon glyphicon-upload";
				break;
			case '35': 
				return "glyphicon glyphicon-inbox";
				break;
			case '36': 
				return "glyphicon glyphicon-play-circle";
				break;
			case '37': 
				return "glyphicon glyphicon-repeat";
				break;
			case '38': 
				return "glyphicon glyphicon-refresh";
				break;
			case '39': 
				return "glyphicon glyphicon-list-alt";
				break;
			case '40': 
				return "glyphicon glyphicon-lock";
				break;
			case '41': 
				return "glyphicon glyphicon-flag";
				break;
			case '42': 
				return "glyphicon glyphicon-headphones";
				break;
			case '43': 
				return "glyphicon glyphicon-volume-off";
				break;
			case '44': 
				return "glyphicon glyphicon-volume-down";
				break;
			case '45': 
				return "glyphicon glyphicon-volume-up";
				break;
			case '46': 
				return "glyphicon glyphicon-qrcode";
				break;
			case '47': 
				return "glyphicon glyphicon-barcode";
				break;
			case '48': 
				return "glyphicon glyphicon-tag";
				break;
			case '49': 
				return "glyphicon glyphicon-tags";
				break;
			case '50': 
				return "glyphicon glyphicon-book";
				break;
			case '51': 
				return "glyphicon glyphicon-bookmark";
				break;
			case '52': 
				return "glyphicon glyphicon-print";
				break;
			case '53': 
				return "glyphicon glyphicon-camera";
				break;
			case '54': 
				return "glyphicon glyphicon-font";
				break;
			case '55': 
				return "glyphicon glyphicon-bold";
				break;
			case '56': 
				return "glyphicon glyphicon-italic";
				break;
			case '57': 
				return "glyphicon glyphicon-text-height";
				break;
			case '58': 
				return "glyphicon glyphicon-text-width";
				break;
			case '59': 
				return "glyphicon glyphicon-align-left";
				break;
			case '60': 
				return "glyphicon glyphicon-align-center";
				break;
			case '61': 
				return "glyphicon glyphicon-align-right";
				break;
			case '62': 
				return "glyphicon glyphicon-align-justify";
				break;
			case '63': 
				return "glyphicon glyphicon-list";
				break;
			case '64': 
				return "glyphicon glyphicon-indent-left";
				break;
			case '65': 
				return "glyphicon glyphicon-indent-right";
				break;
			case '66': 
				return "glyphicon glyphicon-facetime-video";
				break;
			case '67': 
				return "glyphicon glyphicon-picture";
				break;
			case '68': 
				return "glyphicon glyphicon-map-marker";
				break;
			case '69': 
				return "glyphicon glyphicon-adjust";
				break;
			case '70': 
				return "glyphicon glyphicon-tint";
				break;
			case '71': 
				return "glyphicon glyphicon-edit";
				break;
			case '72': 
				return "glyphicon glyphicon-share";
				break;
			case '73': 
				return "glyphicon glyphicon-check";
				break;
			case '74': 
				return "glyphicon glyphicon-move";
				break;
			case '75': 
				return "glyphicon glyphicon-step-backward";
				break;
			case '76': 
				return "glyphicon glyphicon-fast-backward";
				break;
			case '77': 
				return "glyphicon glyphicon-backward";
				break;
			case '78': 
				return "glyphicon glyphicon-play";
				break;
			case '79': 
				return "glyphicon glyphicon-pause";
				break;
			case '80': 
				return "glyphicon glyphicon-stop";
				break;
			case '81': 
				return "glyphicon glyphicon-forward";
				break;
			case '82': 
				return "glyphicon glyphicon-fast-forward";
				break;
			case '83': 
				return "glyphicon glyphicon-step-forward";
				break;
			case '84': 
				return "glyphicon glyphicon-eject";
				break;
			case '85': 
				return "glyphicon glyphicon-chevron-left";
				break;
			case '86': 
				return "glyphicon glyphicon-chevron-right";
				break;
			case '87': 
				return "glyphicon glyphicon-plus-sign";
				break;
			case '88': 
				return "glyphicon glyphicon-minus-sign";
				break;
			case '89': 
				return "glyphicon glyphicon-remove-sign";
				break;
			case '90': 
				return "glyphicon glyphicon-ok-sign";
				break;
			case '91': 
				return "glyphicon glyphicon-question-sign";
				break;
			case '92': 
				return "glyphicon glyphicon-info-sign";
				break;
			case '93': 
				return "glyphicon glyphicon-screenshot";
				break;
			case '94': 
				return "glyphicon glyphicon-remove-circle";
				break;
			case '95': 
				return "glyphicon glyphicon-ok-circle";
				break;
			case '96': 
				return "glyphicon glyphicon-ban-circle";
				break;
			case '97': 
				return "glyphicon glyphicon-arrow-left";
				break;
			case '98': 
				return "glyphicon glyphicon-arrow-right";
				break;
			case '99': 
				return "glyphicon glyphicon-arrow-up";
				break;
			case '100': 
				return "glyphicon glyphicon-arrow-down";
				break;
			case '101': 
				return "glyphicon glyphicon-share-alt";
				break;
			case '102': 
				return "glyphicon glyphicon-resize-full";
				break;
			case '103': 
				return "glyphicon glyphicon-resize-small";
				break;
			case '104': 
				return "glyphicon glyphicon-exclamation-sign";
				break;
			case '105': 
				return "glyphicon glyphicon-gift";
				break;
			case '106': 
				return "glyphicon glyphicon-leaf";
				break;
			case '107': 
				return "glyphicon glyphicon-fire";
				break;
			case '108': 
				return "glyphicon glyphicon-eye-open";
				break;
			case '109': 
				return "glyphicon glyphicon-eye-close";
				break;
			case '110': 
				return "glyphicon glyphicon-warning-sign";
				break;
			case '111': 
				return "glyphicon glyphicon-plane";
				break;
			case '112': 
				return "glyphicon glyphicon-calendar";
				break;
			case '113': 
				return "glyphicon glyphicon-random";
				break;
			case '114': 
				return "glyphicon glyphicon-comment";
				break;
			case '115': 
				return "glyphicon glyphicon-magnet";
				break;
			case '116': 
				return "glyphicon glyphicon-chevron-up";
				break;
			case '117': 
				return "glyphicon glyphicon-chevron-down";
				break;
			case '118': 
				return "glyphicon glyphicon-retweet";
				break;
			case '119': 
				return "glyphicon glyphicon-shopping-cart";
				break;
			case '120': 
				return "glyphicon glyphicon-folder-close";
				break;
			case '121': 
				return "glyphicon glyphicon-folder-open";
				break;
			case '122': 
				return "glyphicon glyphicon-resize-vertical";
				break;
			case '123': 
				return "glyphicon glyphicon-resize-horizontal";
				break;
			case '124': 
				return "glyphicon glyphicon-hdd";
				break;
			case '125': 
				return "glyphicon glyphicon-bullhorn";
				break;
			case '126': 
				return "glyphicon glyphicon-bell";
				break;
			case '127': 
				return "glyphicon glyphicon-certificate";
				break;
			case '128': 
				return "glyphicon glyphicon-thumbs-up";
				break;
			case '129': 
				return "glyphicon glyphicon-thumbs-down";
				break;
			case '130': 
				return "glyphicon glyphicon-hand-right";
				break;
			case '131': 
				return "glyphicon glyphicon-hand-left";
				break;
			case '132': 
				return "glyphicon glyphicon-hand-up";
				break;
			case '133': 
				return "glyphicon glyphicon-hand-down";
				break;
			case '134': 
				return "glyphicon glyphicon-circle-arrow-right";
				break;
			case '135': 
				return "glyphicon glyphicon-circle-arrow-left";
				break;
			case '136': 
				return "glyphicon glyphicon-circle-arrow-up";
				break;
			case '137': 
				return "glyphicon glyphicon-circle-arrow-down";
				break;
			case '138': 
				return "glyphicon glyphicon-globe";
				break;
			case '139': 
				return "glyphicon glyphicon-wrench";
				break;
			case '140': 
				return "glyphicon glyphicon-tasks";
				break;
			case '141': 
				return "glyphicon glyphicon-filter";
				break;
			case '142': 
				return "glyphicon glyphicon-briefcase";
				break;
			case '143': 
				return "glyphicon glyphicon-fullscreen";
				break;
			case '144': 
				return "glyphicon glyphicon-dashboard";
				break;
			case '145': 
				return "glyphicon glyphicon-paperclip";
				break;
			case '146': 
				return "glyphicon glyphicon-heart-empty";
				break;
			case '147': 
				return "glyphicon glyphicon-link";
				break;
			case '148': 
				return "glyphicon glyphicon-phone";
				break;
			case '149': 
				return "glyphicon glyphicon-pushpin";
				break;
			case '150': 
				return "glyphicon glyphicon-usd";
				break;
			case '151': 
				return "glyphicon glyphicon-gbp";
				break;
			case '152': 
				return "glyphicon glyphicon-sort";
				break;
			case '153': 
				return "glyphicon glyphicon-sort-by-alphabet";
				break;
			case '154': 
				return "glyphicon glyphicon-sort-by-alphabet-alt";
				break;
			case '155': 
				return "glyphicon glyphicon-sort-by-order";
				break;
			case '156': 
				return "glyphicon glyphicon-sort-by-order-alt";
				break;
			case '157': 
				return "glyphicon glyphicon-sort-by-attributes";
				break;
			case '158': 
				return "glyphicon glyphicon-sort-by-attributes-alt";
				break;
			case '159': 
				return "glyphicon glyphicon-unchecked";
				break;
			case '160': 
				return "glyphicon glyphicon-expand";
				break;
			case '161': 
				return "glyphicon glyphicon-collapse-down";
				break;
			case '162': 
				return "glyphicon glyphicon-collapse-up";
				break;
			case '163': 
				return "glyphicon glyphicon-log-in";
				break;
			case '164': 
				return "glyphicon glyphicon-flash";
				break;
			case '165': 
				return "glyphicon glyphicon-log-out";
				break;
			case '166': 
				return "glyphicon glyphicon-new-window";
				break;
			case '167': 
				return "glyphicon glyphicon-record";
				break;
			case '168': 
				return "glyphicon glyphicon-save";
				break;
			case '169': 
				return "glyphicon glyphicon-open";
				break;
			case '170': 
				return "glyphicon glyphicon-saved";
				break;
			case '171': 
				return "glyphicon glyphicon-import";
				break;
			case '172': 
				return "glyphicon glyphicon-export";
				break;
			case '173': 
				return "glyphicon glyphicon-send";
				break;
			case '174': 
				return "glyphicon glyphicon-floppy-disk";
				break;
			case '175': 
				return "glyphicon glyphicon-floppy-saved";
				break;
			case '176': 
				return "glyphicon glyphicon-floppy-remove";
				break;
			case '177': 
				return "glyphicon glyphicon-floppy-save";
				break;
			case '178': 
				return "glyphicon glyphicon-floppy-open";
				break;
			case '179': 
				return "glyphicon glyphicon-credit-card";
				break;
			case '180': 
				return "glyphicon glyphicon-transfer";
				break;
			case '181': 
				return "glyphicon glyphicon-cutlery";
				break;
			case '182': 
				return "glyphicon glyphicon-header";
				break;
			case '183': 
				return "glyphicon glyphicon-compressed";
				break;
			case '184': 
				return "glyphicon glyphicon-earphone";
				break;
			case '185': 
				return "glyphicon glyphicon-phone-alt";
				break;
			case '186': 
				return "glyphicon glyphicon-tower";
				break;
			case '187': 
				return "glyphicon glyphicon-stats";
				break;
			case '188': 
				return "glyphicon glyphicon-sd-video";
				break;
			case '189': 
				return "glyphicon glyphicon-hd-video";
				break;
			case '190': 
				return "glyphicon glyphicon-subtitles";
				break;
			case '191': 
				return "glyphicon glyphicon-sound-stereo";
				break;
			case '192': 
				return "glyphicon glyphicon-sound-dolby";
				break;
			case '193': 
				return "glyphicon glyphicon-sound-5-1";
				break;
			case '194': 
				return "glyphicon glyphicon-sound-6-1";
				break;
			case '195': 
				return "glyphicon glyphicon-sound-7-1";
				break;
			case '196': 
				return "glyphicon glyphicon-copyright-mark";
				break;
			case '197': 
				return "glyphicon glyphicon-registration-mark";
				break;
			case '198': 
				return "glyphicon glyphicon-cloud-download";
				break;
			case '199': 
				return "glyphicon glyphicon-cloud-upload";
				break;
			case '200': 
				return "glyphicon glyphicon-tree-conifer";
				break;
			case '201': 
				return "glyphicon glyphicon-tree-deciduous";
				break;
			case '202': 
				return "glyphicon glyphicon-cd";
				break;
			case '203': 
				return "glyphicon glyphicon-save-file";
				break;
			case '204': 
				return "glyphicon glyphicon-open-file";
				break;
			case '205': 
				return "glyphicon glyphicon-level-up";
				break;
			case '206': 
				return "glyphicon glyphicon-copy";
				break;
			case '207': 
				return "glyphicon glyphicon-paste";
				break;
			case '208': 
				return "glyphicon glyphicon-alert";
				break;
			case '209': 
				return "glyphicon glyphicon-equalizer";
				break;
			case '210': 
				return "glyphicon glyphicon-king";
				break;
			case '211': 
				return "glyphicon glyphicon-queen";
				break;
			case '212': 
				return "glyphicon glyphicon-pawn";
				break;
			case '213': 
				return "glyphicon glyphicon-bishop";
				break;
			case '214': 
				return "glyphicon glyphicon-knight";
				break;
			case '215': 
				return "glyphicon glyphicon-baby-formula";
				break;
			case '216': 
				return "glyphicon glyphicon-tent";
				break;
			case '217': 
				return "glyphicon glyphicon-blackboard";
				break;
			case '218': 
				return "glyphicon glyphicon-bed";
				break;
			case '219': 
				return "glyphicon glyphicon-apple";
				break;
			case '220': 
				return "glyphicon glyphicon-erase";
				break;
			case '221': 
				return "glyphicon glyphicon-hourglass";
				break;
			case '222': 
				return "glyphicon glyphicon-lamp";
				break;
			case '223': 
				return "glyphicon glyphicon-duplicate";
				break;
			case '224': 
				return "glyphicon glyphicon-piggy-bank";
				break;
			case '225': 
				return "glyphicon glyphicon-scissors";
				break;
			case '226': 
				return "glyphicon glyphicon-bitcoin";
				break;
			case '227': 
				return "glyphicon glyphicon-btc";
				break;
			case '228': 
				return "glyphicon glyphicon-xbt";
				break;
			case '229': 
				return "glyphicon glyphicon-yen";
				break;
			case '230': 
				return "glyphicon glyphicon-jpy";
				break;
			case '231': 
				return "glyphicon glyphicon-ruble";
				break;
			case '232': 
				return "glyphicon glyphicon-rub";
				break;
			case '233': 
				return "glyphicon glyphicon-scale";
				break;
			case '234': 
				return "glyphicon glyphicon-ice-lolly";
				break;
			case '235': 
				return "glyphicon glyphicon-ice-lolly-tasted";
				break;
			case '236': 
				return "glyphicon glyphicon-education";
				break;
			case '237': 
				return "glyphicon glyphicon-option-horizontal";
				break;
			case '238': 
				return "glyphicon glyphicon-option-vertical";
				break;
			case '239': 
				return "glyphicon glyphicon-menu-hamburger";
				break;
			case '240': 
				return "glyphicon glyphicon-modal-window";
				break;
			case '241': 
				return "glyphicon glyphicon-oil";
				break;
			case '242': 
				return "glyphicon glyphicon-grain";
				break;
			case '243': 
				return "glyphicon glyphicon-sunglasses";
				break;
			case '244': 
				return "glyphicon glyphicon-text-size";
				break;
			case '245': 
				return "glyphicon glyphicon-text-color";
				break;
			case '246': 
				return "glyphicon glyphicon-text-background";
				break;
			case '247': 
				return "glyphicon glyphicon-object-align-top";
				break;
			case '248': 
				return "glyphicon glyphicon-object-align-bottom";
				break;
			case '249': 
				return "glyphicon glyphicon-object-align-horizontal";
				break;
			case '250': 
				return "glyphicon glyphicon-object-align-left";
				break;
			case '251': 
				return "glyphicon glyphicon-object-align-vertical";
				break;
			case '252': 
				return "glyphicon glyphicon-object-align-right";
				break;
			case '253': 
				return "glyphicon glyphicon-triangle-right";
				break;
			case '254': 
				return "glyphicon glyphicon-triangle-left";
				break;
			case '255': 
				return "glyphicon glyphicon-triangle-bottom";
				break;
			case '256': 
				return "glyphicon glyphicon-triangle-top";
				break;
			case '257': 
				return "glyphicon glyphicon-console";
				break;
			case '258': 
				return "glyphicon glyphicon-superscript";
				break;
			case '259': 
				return "glyphicon glyphicon-subscript";
				break;
			case '260': 
				return "glyphicon glyphicon-menu-left";
				break;
			case '261': 
				return "glyphicon glyphicon-menu-right";
				break;
			case '262': 
				return "glyphicon glyphicon-menu-down";
				break;
			case '263': 
				return "glyphicon glyphicon-menu-up";
				break;
			default:
				break;
		}
	}
?>