<?php
include("conexion.php");

if(!isset($_GET['SITE'])||!isset($_GET['TABLE']))
{
	header("Location: SITIOS.php");
}
else
{
	$site=$_GET['SITE'];
	$table=$_GET['TABLE'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Creathor</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Datos del campos &raquo; Agregar datos</h2>
			<hr />

			<?php
			if(isset($_POST['campo_add']))
			{
				$clave_campo	     = mysqli_real_escape_string($con,(strip_tags($_POST["clave_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_campo	 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$update_at			 = date("Y-m-d H:i:s"); 
				$create_at	 		 = date("Y-m-d H:i:s"); 
				$tipo_campo			 = mysqli_real_escape_string($con,(strip_tags($_POST["tipo_campo"],ENT_QUOTES)));//Escanpando caracteres 
				
				$longitud_campo		 = mysqli_real_escape_string($con,(strip_tags($_POST["longitud_campo"],ENT_QUOTES)));//Escanpando caracteres 
				$carpeta			 = mysqli_real_escape_string($con,(strip_tags($_POST["carpeta"],ENT_QUOTES)));//Escanpando 

				$visible			 = isset($_POST["visible"])  ?((mysqli_real_escape_string($con,(strip_tags($_POST["visible"],ENT_QUOTES)))=='on')?1:0):0;
				$editable			 = isset($_POST["editable"]) ?((mysqli_real_escape_string($con,(strip_tags($_POST["editable"],ENT_QUOTES)))=='on')?1:0):0;
				$requerido			 = isset($_POST["requerido"])?((mysqli_real_escape_string($con,(strip_tags($_POST["requerido"],ENT_QUOTES)))=='on')?1:0):0;
				$indice				 = isset($_POST["indice"]) 	?((mysqli_real_escape_string($con,(strip_tags($_POST["indice"],ENT_QUOTES)))=='on')?1:0):0;
				$busqueda			 = isset($_POST["busqueda"]) ?((mysqli_real_escape_string($con,(strip_tags($_POST["busqueda"],ENT_QUOTES)))=='on')?1:0):0;

				$jpeg                = isset($_POST["jpeg"])?  ((mysqli_real_escape_string($con,(strip_tags($_POST["jpeg"],ENT_QUOTES)))=='on')?1:0):0;
				$png                 = isset($_POST["png"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["png"],ENT_QUOTES)))=='on')?1:0):0;
				$gif                 = isset($_POST["gif"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["gif"],ENT_QUOTES)))=='on')?1:0):0;
				$tif                 = isset($_POST["tif"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["tif"],ENT_QUOTES)))=='on')?1:0):0;
				$svg                 = isset($_POST["svg"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["svg"],ENT_QUOTES)))=='on')?1:0):0;
				$eps                 = isset($_POST["eps"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["eps"],ENT_QUOTES)))=='on')?1:0):0;
				$pdf                 = isset($_POST["pdf"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["pdf"],ENT_QUOTES)))=='on')?1:0):0;
				$xlsx                = isset($_POST["xlsx"])?  ((mysqli_real_escape_string($con,(strip_tags($_POST["xlsx"],ENT_QUOTES)))=='on')?1:0):0;
				$xlsm                = isset($_POST["xlsm"])?  ((mysqli_real_escape_string($con,(strip_tags($_POST["xlsm"],ENT_QUOTES)))=='on')?1:0):0;
				$doc                 = isset($_POST["doc"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["doc"],ENT_QUOTES)))=='on')?1:0):0;
				$docx                = isset($_POST["docx"])?  ((mysqli_real_escape_string($con,(strip_tags($_POST["docx"],ENT_QUOTES)))=='on')?1:0):0;
				$dwg                 = isset($_POST["dwg"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["dwg"],ENT_QUOTES)))=='on')?1:0):0;
				$txt                 = isset($_POST["txt"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["txt"],ENT_QUOTES)))=='on')?1:0):0;
				$zip                 = isset($_POST["zip"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["zip"],ENT_QUOTES)))=='on')?1:0):0;
				$rar                 = isset($_POST["rar"]) ?  ((mysqli_real_escape_string($con,(strip_tags($_POST["rar"],ENT_QUOTES)))=='on')?1:0):0;
			

				$cek = mysqli_query($con, "SELECT * FROM campos WHERE id_sitio=$site and id_tabla=$table and clave_campo='$clave_campo'");
				if(mysqli_num_rows($cek) == 0)
				{
						$insert = mysqli_query($con, "INSERT INTO campos(id_sitio, id_tabla, clave_campo, descripcion_campo, create_at, update_at,  tipo_campo, longitud_campo,visible, editable, requerido, indice, busqueda, carpeta, jpeg, png ,gif ,tif ,svg ,eps ,pdf ,xlsx, xlsm, doc ,docx, dwg, txt ,zip ,rar) VALUES('$site','$table','$clave_campo', '$descripcion_campo','$create_at', '$update_at', '$tipo_campo','$longitud_campo','$visible', '$editable','$requerido','$indice','$busqueda', '$carpeta','$jpeg','$png','$gif','$tif','$svg','$eps','$pdf','$xlsx','$xlsm','$doc','$docx','$dwg','$txt','$zip','$rar');") or die(mysqli_error());
						if($insert){
							echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
							
							header("Location: CAMPOS.php?SITE=$site&TABLE=$table");
						}else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
						}
					 
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. Clave_campo exite!</div>';
				}
			}
			?>

			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">Clave_campo</label>
					<div class="col-sm-2">
						<input type="text" name="clave_campo" class="form-control" placeholder="Clave_campo" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">descripcion_campo</label>
					<div class="col-sm-4">
						<input type="text" name="descripcion_campo" class="form-control" placeholder="descripcion_campo" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">tipo_campo</label>
					<div class="col-sm-3">
						<select name="tipo_campo" class="form-control">
							<option value=""> ----- </option>
							<option value="1">INT</option>
							<option value="2">VARCHAR</option>
							<option value="3">DATE</option>
							<option value="4">DATETIME</option>
							<option value="5">TEXT</option>
							<option value="6">FILE</option>
							<option value="7">BOOLEAN</option>
							<option value="8">DOUBLE</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">longitud_campo</label>
					<div class="col-sm-4">
						<input type="number" name="longitud_campo" class="form-control" placeholder="longitud_campo" required>
					</div>
				</div>


				<div class="form-group" >
					<label class="col-sm-3 control-label">carpeta</label>
					<div class="col-sm-4">
						<input type="text" name="carpeta" id="carpeta" value="" class="form-control" placeholder="carpeta" disabled >
					</div>
				</div>






				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">jpeg</label>
						<input type="checkbox" name="jpeg" class="form-control" placeholder="jpeg" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">png</label>
						<input type="checkbox" name="png" class="form-control" placeholder="png" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">gif</label>
						<input type="checkbox" name="gif" class="form-control" placeholder="gif" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">tif</label>
						<input type="checkbox" name="tif" class="form-control" placeholder="tif" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">svg</label>
						<input type="checkbox" name="svg" class="form-control" placeholder="svg" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">eps</label>
						<input type="checkbox" name="eps" class="form-control" placeholder="eps" >
					</div>
					</center>
				</div>
				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">pdf</label>
						<input type="checkbox" name="pdf" class="form-control" placeholder="pdf" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">xlsx</label>
						<input type="checkbox" name="xlsx" class="form-control" placeholder="xlsx" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">xlsm</label>
						<input type="checkbox" name="xlsm" class="form-control" placeholder="xlsm" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">doc</label>
						<input type="checkbox" name="doc" class="form-control" placeholder="doc" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">docx</label>
						<input type="checkbox" name="docx" class="form-control" placeholder="docx" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">dwg</label>
						<input type="checkbox" name="dwg" class="form-control" placeholder="dwg" >
					</div>
					</center>
				</div>
				<div class="form-group">
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">txt</label>
						<input type="checkbox" name="txt" class="form-control" placeholder="txt" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">zip</label>
						<input type="checkbox" name="zip" class="form-control" placeholder="zip" >
					</div>
					</center>
					<center>
					<div class="col-sm-1">
						<label class="col-sm-1 control-label">rar</label>
						<input type="checkbox" name="rar" class="form-control" placeholder="rar" >
					</div>
					</center>
				</div>








				<div class="form-group">
					<label class="col-sm-3 control-label">indice</label>
					<div class="col-sm-4">
						<input type="checkbox" name="indice" class="form-control" placeholder="indice">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">visible</label>
					<div class="col-sm-4">
						<input type="checkbox" name="visible" class="form-control" placeholder="visible">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">editable</label>
					<div class="col-sm-4">
						<input type="checkbox" name="editable" class="form-control" placeholder="editable">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">requerido</label>
					<div class="col-sm-4">
						<input type="checkbox" name="requerido" class="form-control" placeholder="requerido">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="campo_add" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="CAMPOS.php?SITE=<?php echo $site; ?>&TABLE=<?php echo $table; ?>" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('select').on('change', function() {
		if( this.value == 6 )
		{
			$( "#carpeta" ).prop("disabled", false);
			$( "#jpeg" ).prop("disabled", false);
			$( "#png" ).prop("disabled", false);
			$( "#gif" ).prop("disabled", false);
			$( "#tif" ).prop("disabled", false);
			$( "#svg" ).prop("disabled", false);
			$( "#eps" ).prop("disabled", false);
			$( "#pdf" ).prop("disabled", false);
			$( "#lsx" ).prop("disabled", false);
			$( "#lsm" ).prop("disabled", false);
			$( "#doc" ).prop("disabled", false);
			$( "#ocx" ).prop("disabled", false);
			$( "#dwg" ).prop("disabled", false);
			$( "#txt" ).prop("disabled", false);
			$( "#zip" ).prop("disabled", false);
			$( "#rar" ).prop("disabled", false);
		}
		else
		{
			$( "#carpeta" ).prop( "disabled", true );
			$( "#jpeg" ).prop("disabled", true);
			$( "#png" ).prop("disabled", true);
			$( "#gif" ).prop("disabled", true);
			$( "#tif" ).prop("disabled", true);
			$( "#svg" ).prop("disabled", true);
			$( "#eps" ).prop("disabled", true);
			$( "#pdf" ).prop("disabled", true);
			$( "#lsx" ).prop("disabled", true);
			$( "#lsm" ).prop("disabled", true);
			$( "#doc" ).prop("disabled", true);
			$( "#ocx" ).prop("disabled", true);
			$( "#dwg" ).prop("disabled", true);
			$( "#txt" ).prop("disabled", true);
			$( "#zip" ).prop("disabled", true);
			$( "#rar" ).prop("disabled", true);
		}
	});

	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
</body>
</html>
